#include "AbstractMap.h"
#include "Functions.h"
#include "PlayWindow.h"
#include <iostream>
#include <queue>
#include <set>
#include <algorithm>

AbstractMap::AbstractMap(Map* map)
{
    this->map = map;
    if (map->getType()==2)
        speed=10;
    else speed = 5;
    vector<double> minDist(map->mapBuildings.size(),2000000000);
    vector<double> minGateDist(map->mapGates.size(),2000000000);
    Robber* c = new Robber(false);
    c->setPos(-15,-15);
    for (int i=0; i<127; i++)
    {
        for (int j=0; j<127; j++)
        {
            QPointF currentPos= c->pos();
            if (c->onMap()){ //record entrance of all buildings (try all vertices)
                robberGrid[i][j].om=true;
                for (unsigned int k=0; k<map->mapBuildings.size(); k++){
                    double x=Functions::calculateDistance(currentPos,map->mapBuildings[k]->pos());
                    if (x < minDist[k]){
                        minDist[k]=x;
                        map->buildingEntrances[k]=currentPos;
                    }
                }
                for (unsigned int k=0; k<map->mapGates.size(); k++){
                    double x=Functions::calculateDistance(currentPos,map->mapGates[k]->pos());
                    if (x < minGateDist[k]){
                        minGateDist[k]=x;
                        map->gateEntrances[k]=currentPos;
                    }
                }
            }

            c->setPos(c->pos().x()+speed, c->pos().y());
            ///////////////
            if (!c->onMap()){
                grid[i][j].right=0;
                robberGrid[i][j].right=0;
            }
            else {
                grid[i][j].right=1;
                robberGrid[i][j].right=1;
            }
            ////////////////
            c->setPos(currentPos);
            c->setPos(c->pos().x()-speed, c->pos().y());
            if (!c->onMap()){
                grid[i][j].left=0;
                robberGrid[i][j].left=0;
            }
            else {
                grid[i][j].left=1;
                robberGrid[i][j].left=1;
            }
            c->setPos(currentPos);
            c->setPos(c->pos().x(), c->pos().y()-speed);
            if (!c->onMap()){
                grid[i][j].up=0;
                robberGrid[i][j].up=0;
            }
            else {
                grid[i][j].up=1;
                robberGrid[i][j].up=1;
            }
            c->setPos(currentPos);
            c->setPos(c->pos().x(), c->pos().y()+speed);
            if (!c->onMap()){
                grid[i][j].down=0;
                robberGrid[i][j].down=0;
            }
            else {
                grid[i][j].down=1;
                robberGrid[i][j].down=1;
            }
            c->setPos(currentPos);
            c->setPos(c->pos().x()+speed,c->pos().y());
        }
        c->setPos(-15, c->pos().y()+speed);
    }
}

void AbstractMap::fillRobberGrid(PoliceDep* policeDep) //O(V), fast
{
    int leastWeight = 2100000000;
    Robber* c = new Robber(false);
    c->setPos(-15,-15);
    c->setVisible(false);
    for (int i=0; i<127; i++)
    {
        for (int j=0; j<127; j++)
        {
            robberGrid[i][j].vWeight=0; //re-init
            QPointF currentPos= c->pos();
            for (unsigned int k=0; k<policeDep->getSize(); k++){
                QPointF policePos = policeDep->getPolice(k)->pos();
                double d = Functions::calculateDistance(currentPos,policePos);
                if ((int)d<200){ //visible to robber
                    robberGrid[i][j].vWeight+=(int)d; //add to vertex weight
                }
                if ((int)d<27){
                    robberGrid[i][j].vWeight=-1;//indicates dont come here ever
                }
            }
            if (robberGrid[i][j].om && robberGrid[i][j].vWeight!=-1
                    && robberGrid[i][j].vWeight<leastWeight){
                leastWeight=robberGrid[i][j].vWeight;
                leastWeightV=make_pair(i,j);
            }
            c->setPos(c->pos().x()+speed,c->pos().y());
        }
        c->setPos(-15, c->pos().y()+speed);
    }
}

QPointF AbstractMap::BFS(QPointF p, QPointF r)
{
    int x1 = (p.x()+15)/speed;
    int y1 = (p.y()+15)/speed;
    int x2 = (r.x()+15)/speed;
    int y2 = (r.y()+15)/speed;

    if ((x1==x2 && y1==y2) || !robberGrid[y2][x2].om)
        return QPointF(-50,-50); //game over

    //initialization
    for (int i=0; i<127; i++)
    {
        for (int j=0; j<127; j++)
        {
            grid[i][j].color=0;
            grid[i][j].distance=2000000000;
            grid[i][j].parent=make_pair(-1,-1);
        }
    }

    queue<pair<int,int> > Q;
    Q.push(make_pair(x1,y1));

    grid[y1][x1].color=1;
    grid[y1][x1].distance=0;

    while(!Q.empty())
    {
        pair<int,int> u = Q.front();
        Q.pop();
        if (grid[u.second][u.first].left==1) {
            pair<int,int> v = make_pair(u.first-1,u.second);
            if (grid[v.second][v.first].color==0) {
                grid[v.second][v.first].color=1;
                grid[v.second][v.first].distance=1+grid[u.second][u.first].distance;
                grid[v.second][v.first].parent=make_pair(u.first,u.second);
                Q.push(v);
            }
        }
        if (grid[u.second][u.first].right==1) {
            pair<int,int> v = make_pair(u.first+1,u.second);
            if (grid[v.second][v.first].color==0) {
                grid[v.second][v.first].color=1;
                grid[v.second][v.first].distance=1+grid[u.second][u.first].distance;
                grid[v.second][v.first].parent=make_pair(u.first,u.second);
                Q.push(v);
            }
        }
        if (grid[u.second][u.first].up==1) {
            pair<int,int> v = make_pair(u.first,u.second-1);
            if (grid[v.second][v.first].color==0) {
                grid[v.second][v.first].color=1;
                grid[v.second][v.first].distance=1+grid[u.second][u.first].distance;
                grid[v.second][v.first].parent=make_pair(u.first,u.second);
                Q.push(v);
            }
        }
        if (grid[u.second][u.first].down==1) {
            pair<int,int> v = make_pair(u.first,u.second+1);
            if (grid[v.second][v.first].color==0) {
                grid[v.second][v.first].color=1;
                grid[v.second][v.first].distance=1+grid[u.second][u.first].distance;
                grid[v.second][v.first].parent=make_pair(u.first,u.second);
                Q.push(v);
            }
        }
        grid[u.second][u.first].color=2;
    } //end of BFS, we now have all parent pointers & distances needed

    pair<int,int> t; //backtrack
    int xn=-1;
    int yn=-1;
    while (x2!=x1 || y2!=y1)
    {
        t = grid[y2][x2].parent;
        xn=x2;
        yn=y2;
        x2=t.first;
        y2=t.second;
    }
    return QPointF(xn*speed-15,yn*speed-15);
}

//takes vertex weights into account.
QPointF AbstractMap::dijkstra(QPointF start) //E lgV dijkstra. graph sparse->V lgV
{
    int x = (start.x()+15)/speed;
    int y = (start.y()+15)/speed;
    //initialization
    for (int i=0; i<127; i++)
    {
        for (int j=0; j<127; j++)
        {
            robberGrid[i][j].distance=2100000000;
            robberGrid[i][j].parent=make_pair(-1,-1);
        }
    }
    //y x since coords of qgraphics scene are inverted wrt i j
    robberGrid[y][x].distance=0;
    std::set< pair<int,pair<int,int> > > Q; //weight, coords
    Q.insert(make_pair(0,make_pair(x,y)));
    while (!Q.empty()){
        pair<int,int> newVertex = Q.begin()->second; //get the minimum pair
        Q.erase(Q.begin());
        if (robberGrid[newVertex.second][newVertex.first].up==1){ //if reachable
            pair<int,int> upV = make_pair(newVertex.first,newVertex.second-1);
            int edgeWeight = 1+robberGrid[upV.second][upV.first].vWeight;
            int currWeight = robberGrid[newVertex.second][newVertex.first].distance;
            if (edgeWeight!=0 && currWeight + edgeWeight < robberGrid[upV.second][upV.first].distance){
                int oldDistance = robberGrid[upV.second][upV.first].distance;
                robberGrid[upV.second][upV.first].distance=currWeight+edgeWeight;
                if (oldDistance==2100000000) //not previously relaxed
                    Q.insert(make_pair(currWeight+edgeWeight,upV));
                else {
                    Q.erase(Q.find(make_pair(oldDistance,upV)));
                    Q.insert(make_pair(currWeight+edgeWeight,upV));
                }
                robberGrid[upV.second][upV.first].parent=newVertex;
            }
        }
        if (robberGrid[newVertex.second][newVertex.first].down==1){ //if reachable
            pair<int,int> downV = make_pair(newVertex.first,newVertex.second+1);
            int edgeWeight = 1+robberGrid[downV.second][downV.first].vWeight;
            int currWeight = robberGrid[newVertex.second][newVertex.first].distance;
            if (edgeWeight!=0 && currWeight + edgeWeight < robberGrid[downV.second][downV.first].distance){
                int oldDistance = robberGrid[downV.second][downV.first].distance;
                robberGrid[downV.second][downV.first].distance=currWeight+edgeWeight;
                if (oldDistance==2100000000) //not previously relaxed
                    Q.insert(make_pair(currWeight+edgeWeight,downV));
                else {
                    Q.erase(Q.find(make_pair(oldDistance,downV)));
                    Q.insert(make_pair(currWeight+edgeWeight,downV));
                }
                robberGrid[downV.second][downV.first].parent=newVertex;
            }
        }
        if (robberGrid[newVertex.second][newVertex.first].left==1){ //if reachable
            pair<int,int> leftV = make_pair(newVertex.first-1,newVertex.second);
            int edgeWeight = 1+robberGrid[leftV.second][leftV.first].vWeight;
            int currWeight = robberGrid[newVertex.second][newVertex.first].distance;
            if (edgeWeight!=0 && currWeight + edgeWeight < robberGrid[leftV.second][leftV.first].distance){
                int oldDistance = robberGrid[leftV.second][leftV.first].distance;
                robberGrid[leftV.second][leftV.first].distance=currWeight+edgeWeight;
                if (oldDistance==2100000000) //not previously relaxed
                    Q.insert(make_pair(currWeight+edgeWeight,leftV));
                else {
                    Q.erase(Q.find(make_pair(oldDistance,leftV)));
                    Q.insert(make_pair(currWeight+edgeWeight,leftV));
                }
                robberGrid[leftV.second][leftV.first].parent=newVertex;
            }
        }
        if (robberGrid[newVertex.second][newVertex.first].right==1){ //if reachable
            pair<int,int> rightV = make_pair(newVertex.first+1,newVertex.second);
            int edgeWeight = 1+robberGrid[rightV.second][rightV.first].vWeight;
            int currWeight = robberGrid[newVertex.second][newVertex.first].distance;
            if (edgeWeight!=0 && currWeight + edgeWeight < robberGrid[rightV.second][rightV.first].distance){
                int oldDistance = robberGrid[rightV.second][rightV.first].distance;
                robberGrid[rightV.second][rightV.first].distance=currWeight+edgeWeight;
                if (oldDistance==2100000000) //not previously relaxed
                    Q.insert(make_pair(currWeight+edgeWeight,rightV));
                else {
                    Q.erase(Q.find(make_pair(oldDistance,rightV)));
                    Q.insert(make_pair(currWeight+edgeWeight,rightV));
                }
                robberGrid[rightV.second][rightV.first].parent=newVertex;
            }
        }
    }//end of dijkstra, min dists are now in place

    int minDist = 10000;
    int fbx=-1,fby=-1;
    //find building to go to (one with least weight)
    for (unsigned int k=0; k<map->buildingEntrances.size(); k++) {
        if (map->mapBuildings[k]->getMoney()==0
                || (map->mapBuildings[k]->isGuarded()==2
                    && Functions::calculateDistance(start,map->mapBuildings[k]->pos())))
            continue;
        int bx = (map->buildingEntrances[k].x()+15)/speed;
        int by = (map->buildingEntrances[k].y()+15)/speed;
        if (robberGrid[by][bx].distance<minDist){
            minDist=robberGrid[by][bx].distance;
            fbx = bx;
            fby=by;
        }
    }

    minDist = 100000;
    if (PlayWindow::robberScore > map->winScore){//check out a gate if robber has enough score
        for (unsigned int k=0; k<map->mapGates.size(); k++){
            int gx = (map->gateEntrances[k].x()+15)/speed;
            int gy = (map->gateEntrances[k].y()+15)/speed;
            if (robberGrid[gy][gx].distance<minDist){
                minDist=robberGrid[gy][gx].distance;
                fbx = gx;
                fby = gy;
            }
        }
    }

    if (fbx==-1 || fby==-1) { //try to go to safest pos
        fbx=leastWeightV.second;
        fby=leastWeightV.first;
    }
    pair<int,int> t; //backtrack
    int xn=x;
    int yn=y;
    while (fbx!=x || fby!=y)
    {
        t = robberGrid[fby][fbx].parent;
        xn=fbx;
        yn=fby;
        fbx=t.first;
        fby=t.second;
    }
    return QPointF(xn*speed-15,yn*speed-15);
}

