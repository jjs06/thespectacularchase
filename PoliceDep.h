/**
\file PoliceDep.h
\brief A department of several police officers.

Contains a vector of policeMen and another vector indicating the best position for each Police to go.
The position is based on how much the department as a whole knows of the Robber.
*/

#ifndef POLICEDEP_H
#define POLICEDEP_H

#include <QtGui>
#include <vector>
#include "Police.h"

class PoliceDep
{
private:
    vector <Police*> policeMen;
    vector <QPointF> policePos;
public:
    bool tPressed;
    int specialRemaining;

    /**
      Constructs a Police department.
      */
    PoliceDep();
    /**
      Adds a Police to the department.
      */
    void addPolice(Police* p);
    /**
      Sets the location that Police i wants to go to.
      */
    void setLoc(int i, QPointF loc);
    /**
      Retrieves the Police at index pos.
      */
    Police* getPolice(unsigned int pos);
    /**
      Retrieves the location the Police at index i wants to go to.
      */
    QPointF getLoc(unsigned int i);
    /**
      Returns the number of Police in the department.
      */
    unsigned int getSize();
    /**
      Adds all Police to the given scene.
      */
    void addPoliceToScene(QGraphicsScene* scene);
};

#endif // POLICEDEP_H
