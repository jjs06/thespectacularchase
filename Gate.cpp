#include "Gate.h"
#include <string>
using namespace std;

Gate::Gate()
{
    this->setPixmap(QPixmap(string("photos/gate").c_str()).scaled(70,50));
}

QPointF Gate::pos() const
{
    QRectF r = this->boundingRect();
    return QPointF(QGraphicsPixmapItem::pos().x()+r.width()/2,QGraphicsPixmapItem::pos().y()+r.height()/2);
}

void Gate::setPos(qreal x, qreal y)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(x-r.width()/2,y-r.height()/2);
}

void Gate::setPos (const QPointF & pos)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(pos.x()-r.width()/2,pos.y()-r.height()/2);
}
