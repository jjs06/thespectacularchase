/**
\file Flag.h
\brief A flag item.

The flag is used as a marker for places the player would like to remember.
*/

#ifndef FLAG_H
#define FLAG_H

#include <QtGui>

class Flag: public QGraphicsPixmapItem
{
public:
    Flag();
    /**
      Retrieve the position of the Flag.
    */
    QPointF pos() const;
    /**
      Set the Flag position given a QPointF.
      */
    void setPos(const QPointF &pos);
    /**
      Set the Flag position given the x and y values.
      */
    void setPos(qreal x, qreal y);
};

#endif // FLAG_H
