/**
\file Vertex.h
\brief A Vertex used in the 2D Grid representation of the Map.

Each Vertex has left, right, up, and down:
-If equal 0, the player in this position cannot go in that direction
-If equal 1, the player in this position can go in that direction

Each Vertex also has color, distance, weight, and parent which are used in AbstractMap (BFS)
*/

#ifndef VERTEX_H
#define VERTEX_H

#include <utility>

using namespace std;

class Vertex
{
public:
    int left;
    int right;
    int up;
    int down;
    int color;
    int distance;
    int vWeight; //vertex weight
    bool om;
    pair<int,int> parent;
    Vertex();

};

#endif // VERTEX_H
