/**
\file AbstractMap.h
\brief A graph representation of the given Map.

It has two grids, one with vertex weights which are updated constantly, and one without.
*/

#ifndef ABSTRACTMAP_H
#define ABSTRACTMAP_H

#include "Vertex.h"
#include "Robber.h"
#include "PoliceDep.h"
#include "Map.h"
#include <cmath>

using namespace std;

class AbstractMap
{
private:
    Map* map;
    Vertex grid[127][127];
    Vertex robberGrid[127][127];
    pair<int,int> leastWeightV;
    int speed;

public:
    /**
      Fills an unweighted, undirected graph based on the 2D map.
      */
    AbstractMap(Map* map);
    /**
      Fills a graph with Vertex weights and edge weights; Vertex weights increase the closer the police are to them.
      */
    void fillRobberGrid(PoliceDep* policeDep);
    /**
    BFS does a Breadth First Search that computes the shortest path, with source point p to destination point r, and returns the next position to go to.
    */
    QPointF BFS(QPointF p, QPointF r); //returns new position
    /**
      Dijkstra Shortest Path Algorithm, modified to work on edge weights and Vertex weights.
      Runs in E*lg(V). Returns the next point for the Robber to go to: either a building whose path avoids police as much as possible, or the safest place on the map if there are no buildings to steal.
      */
    QPointF dijkstra(QPointF start);

};


#endif // ABSTRACTMAP_H
