#-------------------------------------------------
#
# Project created by QtCreator 2014-04-21T13:38:32
#
#-------------------------------------------------

QT       += core gui sql

TARGET = TheSpectacularChase
TEMPLATE = app


SOURCES += main.cpp \
    MainMenu.cpp \
    DbHandler.cpp \
    Functions.cpp \
    PlayWindow.cpp \
    Map.cpp \
    Road.cpp \
    Gate.cpp \
    Building.cpp \
    AbstractMap.cpp \
    Vertex.cpp \
    Robber.cpp \
    PoliceDep.cpp \
    Police.cpp \
    Flag.cpp

HEADERS  += \
    MainMenu.h \
    DbHandler.h \
    Functions.h \
    PlayWindow.h \
    Map.h \
    Road.h \
    Gate.h \
    Building.h \
    AbstractMap.h \
    Vertex.h \
    Robber.h \
    PoliceDep.h \
    Police.h \
    Flag.h

FORMS    += widget.ui
