#include "MainMenu.h"
#include "time.h"

#include <iostream>
using namespace std;
MainMenu::MainMenu()
{
    view = new QGraphicsView();
    scene = new QGraphicsScene();
    newGamePB = new QPushButton("New Game");
    loadGamePB = new QPushButton("Load Game");
    instPB = new QPushButton ("Instructions");
    cityPB = new QPushButton ("Select City");
    acctPB = new QPushButton ("Select Account");
    exitPB = new QPushButton ("Exit");
    robberRB = new QRadioButton("Robber");
    policeRB = new QRadioButton("Police");
    playingModeBL = new QHBoxLayout();
    playingModeW = new QWidget();
    DbHandler::open();

    scene->setSceneRect(-50, -50, 700, 700);

    newGamePB->setGeometry(230, 150, 140, 40);
    newGamePB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(newGamePB);

    robberRB->setGeometry(0,0, 1, 1);
    robberRB->setChecked(true);
    robberRB->setStyleSheet("font-weight: bold; font-size: 15px;");
    policeRB->setGeometry(1,0, 1, 1);
    policeRB->setStyleSheet("font-weight: bold; font-size: 15px;");
    playingModeBL->addWidget(robberRB);
    playingModeBL->addWidget(policeRB);
    playingModeW->setLayout(playingModeBL);
    playingModeW->setGeometry(205,190,180,10);
    playingModeW->setStyleSheet("background-color: transparent;");
    scene->addWidget(playingModeW);

    mapType = 0;
    username = "Default";

    loadGamePB->setGeometry(220, 240, 160, 40);
    loadGamePB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(loadGamePB);

    instPB->setGeometry(220, 290, 160, 40);
    instPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(instPB);

    cityPB->setGeometry(220, 340, 160, 40);
    cityPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(cityPB);

    acctPB->setGeometry(200, 390, 200, 40);
    acctPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(acctPB);

    exitPB->setGeometry(220, 440, 160, 40);
    exitPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    scene->addWidget(exitPB);


    view->setScene(scene);
    view->setBackgroundBrush(QBrush(QColor(100,100,247,200), Qt::SolidPattern));
    view->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);//QGraphicsView::BoundingRectViewportUpdate);
    view->setDragMode(QGraphicsView::ScrollHandDrag);
    view->showMaximized();

    QObject::connect(newGamePB, SIGNAL(clicked()),this,SLOT(newGame()));
    QObject::connect(loadGamePB, SIGNAL(clicked()),this,SLOT(loadGame()));
    QObject::connect(exitPB,SIGNAL(clicked()),this, SLOT(exit()));
    QObject::connect(instPB, SIGNAL(clicked()),this,SLOT(inst()));
    QObject::connect(acctPB,SIGNAL(clicked()),this, SLOT(acct()));
    QObject::connect(cityPB, SIGNAL(clicked()),this,SLOT(city()));
}

bool MainMenu::getPM()  //get the playing mode
{
    return !(robberRB->isChecked());
}

void MainMenu::newGame()
{
    //TODO: make sure a user is selected (Default username)
    int highscore = DbHandler::retrieveScore(username);

    new PlayWindow (username,highscore,mapType,getPM());

    DbHandler::close();
    view->close();
}

void MainMenu::loadGame()
{
    QGraphicsScene *loadGameScene = new QGraphicsScene;
    loadGameScene->setSceneRect(-50,-50,700,700);

    QComboBox *savedGamesCB = new QComboBox;

    vector<QString>* savedGames;
    savedGames = DbHandler::retrieveSavedGames(username);

    for(unsigned int i=0; i<savedGames->size(); i++)
    {
        savedGamesCB->addItem(savedGames->at(i));
    }
    savedGamesCB->setGeometry(300,300,80,30);
    loadGameScene->addWidget(savedGamesCB);

    QLabel* savedGamesCBLabel = new QLabel("Choose Game:");
    savedGamesCBLabel->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    savedGamesCBLabel->setGeometry(120,300,200,30);
    loadGameScene->addWidget(savedGamesCBLabel);

    QPushButton *backPB = new QPushButton("Back");
    backPB->setGeometry(0, 600, 160, 40);
    backPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    loadGameScene->addWidget(backPB);

    QPushButton *startPB = new QPushButton("Resume Game");
    startPB->setGeometry(450, 600, 170, 40);
    startPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    loadGameScene->addWidget(startPB);

    emit updateLoadGameString(savedGamesCB->currentText());
    view->setScene(loadGameScene);

    connect(backPB,SIGNAL(clicked()),this,SLOT(back()));
    connect(startPB, SIGNAL(clicked()), this, SLOT(loadSelectedGame()));
    connect(savedGamesCB,SIGNAL(currentIndexChanged(QString)),this,SLOT(updateLoadGameString(QString)));
}

void MainMenu::inst()
{
    QGraphicsScene *instScene = new QGraphicsScene;
    instScene->setSceneRect(-50,-50,700,700);

    QPixmap *temp;
    QLabel *keysPic = new QLabel;
    QLabel *robberPic = new QLabel;
    QLabel *policePic = new QLabel;
    QLabel *mousePic = new QLabel;
    QLabel *numbersPic = new QLabel;
    QLabel *pic4 = new QLabel;
    QLabel *pic7 = new QLabel;
    QLabel *pic8 = new QLabel;
    QLabel *pic9 = new QLabel;
    QLabel *robber = new QLabel;
    QLabel *police = new QLabel;

    temp = new QPixmap("photos/robber_scaled.png");
    robber->setGeometry(-400,50,200,200);
    robber->setPixmap(*temp);
    robber->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/police_scaled.png");
    police->setGeometry(-400,420,200,200);
    police->setPixmap(*temp);
    police->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr1.png");
    keysPic->setGeometry(-350,0,450,300);
    keysPic->setPixmap(*temp);
    keysPic->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr2.png");
    robberPic->setGeometry(170,40,200,200);
    robberPic->setPixmap(*temp);
    robberPic->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr3.png");
    policePic->setGeometry(440,40,200,200);
    policePic->setPixmap(*temp);
    policePic->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr4.png");
    pic4->setGeometry(740,40,200,200);
    pic4->setPixmap(*temp);
    pic4->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr5.jpg");
    numbersPic->setGeometry(-145,400,200,200);
    numbersPic->setPixmap(*temp);
    numbersPic->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr6.png");
    mousePic->setGeometry(120,407,200,200);
    mousePic->setPixmap(*temp);
    mousePic->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr7.png");
    pic7->setGeometry(440,410,200,200);
    pic7->setPixmap(*temp);
    pic7->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr8.png");
    pic8->setGeometry(730,410,200,200);
    pic8->setPixmap(*temp);
    pic8->setStyleSheet("background-color: transparent;");
    delete temp;

    temp = new QPixmap("photos/instr6.png");
    pic9->setGeometry(800,416,200,200);
    pic9->setPixmap(*temp);
    pic9->setStyleSheet("background-color: transparent;");
    delete temp;

    QTextEdit *playerLabel = new QTextEdit("Robber");
    QTextEdit *playerLabel2 = new QTextEdit("Police");
    QTextEdit *step1 = new QTextEdit ("1- Use the Arrow keys to move.");
    QTextEdit *step2 = new QTextEdit("2- Get close to a building and steal with the space bar.");
    QTextEdit *step3 = new QTextEdit("3- Avoid the police and don't steal guarded buildings!");
    QTextEdit *step4 = new QTextEdit("4- When your score reaches the target, reach a gate to win!");
    QTextEdit *step5 = new QTextEdit ("1- Use the Number keys to select a policeman.");
    QTextEdit *step6 = new QTextEdit("2- Right click with the mouse to move the selected policeman.");
    QTextEdit *step7 = new QTextEdit("3- Guard the buildings and gates from the robber! Catch him to win!");
    QTextEdit *step8 = new QTextEdit("4- Press T and right click with the mouse to move all policemen to the specified location. You can only do this 3 times per level.");
    playerLabel->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px; color: #CF0000;");
    playerLabel->setAlignment(Qt::AlignCenter);
    playerLabel->setReadOnly(true);
    playerLabel2->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px; color: #00CF00;");
    playerLabel2->setAlignment(Qt::AlignCenter);
    playerLabel2->setReadOnly(true);
    step1->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step1->setReadOnly(true);
    step2->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step2->setReadOnly(true);
    step3->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step3->setReadOnly(true);
    step4->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step4->setReadOnly(true);
    step5->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step5->setReadOnly(true);
    step6->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step6->setReadOnly(true);
    step7->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step7->setReadOnly(true);
    step8->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 20px;");
    step8->setReadOnly(true);

    playerLabel->setGeometry(-450, 20, 200, 50);
    playerLabel2->setGeometry(-450, 400, 200, 50);
    step1->setGeometry(-170, -50, 200, 60);
    step2->setGeometry(150, -50, 200, 80);
    step3->setGeometry(430, -50, 200, 80);
    step4->setGeometry(710, -50, 200, 80);
    step5->setGeometry(-170, 300, 200, 80);
    step6->setGeometry(150, 300, 200, 105);
    step7->setGeometry(430, 300, 200, 110);
    step8->setGeometry(710, 290, 300, 130);
    instScene->addWidget(playerLabel);
    instScene->addWidget(playerLabel2);
    instScene->addWidget(keysPic);
    instScene->addWidget(robberPic);
    instScene->addWidget(policePic);
    instScene->addWidget(numbersPic);
    instScene->addWidget(mousePic);
    instScene->addWidget(robber);
    instScene->addWidget(police);
    instScene->addWidget(pic4);
    instScene->addWidget(pic7);
    instScene->addWidget(pic8);
    instScene->addWidget(pic9);
    instScene->addWidget(step1);
    instScene->addWidget(step2);
    instScene->addWidget(step3);
    instScene->addWidget(step4);
    instScene->addWidget(step5);
    instScene->addWidget(step6);
    instScene->addWidget(step7);
    instScene->addWidget(step8);

    QPushButton *backPB = new QPushButton("Back");
    backPB->setGeometry(-450, 600, 160, 40);
    backPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    instScene->addWidget(backPB);

    view->setScene(instScene);

    connect(backPB,SIGNAL(clicked()),this,SLOT(back()));
}

void MainMenu::city()
{
    QGraphicsScene *cityScene = new QGraphicsScene;
    cityScene->setSceneRect(-50,-50,700,700);

    QComboBox *mapCB = new QComboBox;
    mapCB->addItem("Beirut");
    mapCB->addItem("Zahle");
    mapCB->addItem("Nabatiyyeh");
    mapCB->setGeometry(300,300,120,30);
    cityScene->addWidget(mapCB);

    QLabel* mapCBLabel = new QLabel("Choose City:");
    mapCBLabel->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    mapCBLabel->setGeometry(120,300,200,30);
    cityScene->addWidget(mapCBLabel);

    QPushButton *backPB = new QPushButton("Back");
    backPB->setGeometry(600, 600, 160, 40);
    backPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    cityScene->addWidget(backPB);

    view->setScene(cityScene);

    connect(backPB,SIGNAL(clicked()),this,SLOT(back()));
    connect(mapCB,SIGNAL(currentIndexChanged(int)),this,SLOT(updateMapType(int)));
}

void MainMenu::updateMapType(int index)
{
    mapType = index;
}

void MainMenu::updateUsername(QString user)
{
    username = user;
}

void MainMenu::updateLoadGameString(QString loadgame)
{
    this->loadgame = loadgame;
}

void MainMenu::back()
{
    view->setScene(scene);
}

void MainMenu::acct()
{
    QGraphicsScene *acctScene = new QGraphicsScene;
    acctScene->setSceneRect(-50,-50,700,700);

    QComboBox *acctCB = new QComboBox;
    acctCBPointer = acctCB;
    acctCB->addItem("Default");

    vector<QString> *userList = DbHandler::retrieveUsernames();
    for (unsigned int i=0; i<userList->size(); i++)
    {
        acctCB->addItem(userList->at(i));
    }
    acctCB->setGeometry(300,300,160,30);

    QLabel* cbLabel = new QLabel("Choose Account:");
    cbLabel->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    cbLabel->setGeometry(90,300,200,30);
    acctScene->addWidget(cbLabel);

    QLabel* uLabel = new QLabel("Enter your username:");
    uLabel->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    uLabel->setGeometry(50,340,260,30);
    acctScene->addWidget(uLabel);

    uLine = new QLineEdit();
    uLine->setGeometry(330,340,160,30);
    acctScene->addWidget(uLine);

    acctScene->addWidget(acctCB);

    QPushButton *backPB = new QPushButton("Back");
    backPB->setGeometry(0, 600, 160, 40);
    backPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    acctScene->addWidget(backPB);

    QPushButton *startPB = new QPushButton("New Game");
    startPB->setGeometry(450, 600, 160, 40);
    startPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    acctScene->addWidget(startPB);

    view->setScene(acctScene);

    connect(backPB,SIGNAL(clicked()),this,SLOT(back()));
    connect(startPB, SIGNAL(clicked()), this, SLOT(newGame()));
    connect(uLine,SIGNAL(returnPressed()),this, SLOT(addUser()));
    connect(acctCB,SIGNAL(currentIndexChanged(QString)),this,SLOT(updateUsername(QString)));
}

void MainMenu::addUser()
{
    bool alreadyPresent=false;
    QString user = uLine->text();
    for (int i=0; i<acctCBPointer->count(); i++)
    {
        if (acctCBPointer->itemText(i)==user) {
            alreadyPresent=true;
        }
    }
    if (!alreadyPresent){
        DbHandler::addUsername(user);
        acctCBPointer->addItem(user);
        uLine->clear();
    }
}

void MainMenu::loadSelectedGame()
{
    if(!loadgame.isEmpty())
    {
        DbHandler::load(loadgame, username);
        DbHandler::close();
        view->close();
    }
    else
        cout << "No Game Selected" << endl;
}

void MainMenu::exit()
{
    DbHandler::close();
    view->close();
}
