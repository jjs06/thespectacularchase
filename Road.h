/**
\file Road.h
\brief A QGraphicsLineItem to build the roads on the map

*/

#ifndef ROAD_H
#define ROAD_H
#include "QtGui"

class Road: public QGraphicsRectItem
{
private:
    QPoint startPoint;
    QPoint endPoint;
    int width;

public:
    /**
      Constructs a road from the given starting point, to the given ending point, with width/2 on each side of the line.
      */
    Road(QPoint startPoint, QPoint endPoint, int width);

    /**
      Paints a Road having white strips in the middle
    */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

};

#endif // ROAD_H
