/**
\file Gate.h
\brief A gate item.

The robber escapes through one of the gates when he has enough score.
*/

#ifndef GATE_H
#define GATE_H

#include <QtGui>

class Gate: public QGraphicsPixmapItem
{
public:
    Gate();
    /**
      Retrieve the position of the gate
    */
    QPointF pos() const;
    /**
      Set the Gate position given a QPointF.
      */
    void setPos(const QPointF &pos);
    /**
      Set the Gate position given the x and y values.
      */
    void setPos(qreal x, qreal y);
};

#endif // GATE_H
