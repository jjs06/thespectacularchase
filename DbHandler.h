/**
\file DbHandler.h
\brief A class that handles all interactions with the SQL database.

Contains functions to save and load data and usernames.
*/

#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QtSql>
#include <iostream>
using namespace std;

#include "PlayWindow.h"

class DbHandler
{
private:
    static QSqlDatabase db;
public:
    /**
      Establishes a connection with the game database.
      If the database or any tables were not present, it creates them.
      */
    static void open();
    /**
      Saves a new username in the database.
      */
    static void addUsername(QString username);
    /**
      Updates the highscore of the given user in the database.
      */
    static void updateHighScore(QString username, int highscore);
    /**
      Retrieves the highscore of the given user from the database.
      */
    static int retrieveScore(QString username);
    /**
      Retrieves all saved usernames from the database.
      */
    static vector <QString> *retrieveUsernames();
    /**
      Saves all relevant info from the game.
      If a game is previously saved with this name, it is overwritten.
      */
    static void save(PlayWindow* game, QString savedname);
    /**
      Retrieves the list of saved games for the given username.
      */
    static vector <QString> *retrieveSavedGames(QString username);
    /**
      Loads all relevant info from the database in order to resume game.
      */
    static PlayWindow *load(QString savedname, QString username);
    /**
      Closes the database connection.
      */
    static void close();
};

#endif // DBHANDLER_H
