#include "Robber.h"
#include "iostream"
#include "PlayWindow.h"
using namespace std;

Robber::Robber(bool _ableToSteal, QGraphicsScene* _scene, bool playingMode)
{
    setPixmap(QPixmap(string("photos/robber").c_str()).scaled(30,30));
    ableToSteal=_ableToSteal;
    movingLeft = false;
    movingRight = false;
    movingUp = false;
    movingDown = false;
    scene=_scene;
    int mapType = PlayWindow::map->getType();
    toMoveScene=false;
    speed=5;
    if (!playingMode && mapType==2){
        speed=10;
        topX=250;
        toMoveScene=true;
    }
    else topX=-50;
    topY=-50;
}

QRectF Robber::boundingRect() const
{
    return QRectF(0, 0, 30, 30);
}

bool Robber::getAbleToSteal()
{
    return ableToSteal;
}

void Robber::setMovingRight(bool b)
{
    movingRight = b;
}

void Robber::setMovingLeft(bool b)
{
    movingLeft = b;
}

void Robber::setMovingUp(bool b)
{
    movingUp = b;
}

void Robber::setMovingDown(bool b)
{
    movingDown = b;
}

QPointF Robber::pos() const
{
    QRectF r = this->boundingRect();
    return QPointF(QGraphicsPixmapItem::pos().x()+r.width()/2,QGraphicsPixmapItem::pos().y()+r.height()/2);
}


void Robber::setPos(qreal x, qreal y)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(x-r.width()/2,y-r.height()/2);
}

void Robber::setPos (const QPointF & pos)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(pos.x()-r.width()/2,pos.y()-r.height()/2);
}

//checks of circle is on map: returns true if on map
bool Robber::onMap()
{
    for(unsigned int i =0; i<PlayWindow::map->mapRoads.size(); i++){
        if(collidesWithItem(PlayWindow::map->mapRoads[i],Qt::ContainsItemShape)){
            return true;
        }
    }
    return false;
}

void Robber::update()
{
    int oldX=topX,oldY=topY;
    QPointF currentPos = pos();
    QPointF newPos = currentPos;
    if(movingLeft) {
        newPos.setX(newPos.x() - speed);
        topX -= speed;
    }
    if(movingRight) {
        newPos.setX(newPos.x() + speed);
        topX +=speed;
    }
    if(movingUp){
        newPos.setY(newPos.y() - speed);
        topY-=speed;
    }
    if(movingDown){
        newPos.setY(newPos.y() + speed);
        topY+=speed;
    }

    setPos(newPos);
    if(!onMap()){
        setPos(currentPos);
        topX=oldX;
        topY=oldY;
    }
    else if (toMoveScene){
        PlayWindow::targetScoreLabel->setGeometry(800+topX,topY+30,180,20);
        PlayWindow::scoreLabel->setGeometry(topX+800,topY+70,180,20);
        PlayWindow::policeNearbyLabel->setGeometry(topX+800,topY+110,180,20);
        PlayWindow::robberNearbyLabel->setGeometry(topX+800,topY+110,180,20);
        PlayWindow::targetScoreLabel->setGeometry(topX+800,topY+30,180,20);
        PlayWindow::moneyStolenLabel->setGeometry(topX+800,topY+90,180,20);
        PlayWindow::timeLabel->setGeometry(topX+800,topY+50,180,20);
        PlayWindow::pauseMenu->setGeometry(topX+250,topY+220,300,300);
        PlayWindow::gameOverMenu->setGeometry(topX+250,topY+220,300,300);
        scene->setSceneRect(topX,topY,700,700);
    }
}

/*void Robber::keyPressEvent(QKeyEvent *event)
{

    QPointF currentPos= pos();
    int speed=5;

    switch(event->key())
    {
    case Qt::Key_Left:
        movingLeft = true;
        break;

    case Qt::Key_Right:
        movingRight = true;
        break;

    case Qt::Key_Up:
        movingUp = true;
        break;

    case Qt::Key_Down:
        movingDown = true;
        break;
    }

    if (event->key()==Qt::Key_Space)    //trying to steal
    {
        if(ableToSteal)
        {
            pair<int,bool> add = PlayWindow::map->tryToSteal(this->pos());
            if(add.first>0)
            {
                moneyStolen += add.first;
                incrementScore(add.first);
                QString s;
                s.setNum(score);
                s="Robber's score = "+s;
                scoreLabel->setText(s);

            }
            if (add.second)
            {
                guardedPos=this->pos();
            }
        }
    }

    if(event->key()==Qt::Key_F)     //inserting
        PlayWindow::map->setFlag(pos().x(),pos().y());
}

void Robber::keyReleaseEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Left:
        movingLeft = false;
        break;

    case Qt::Key_Right:
        movingRight = false;
        break;

    case Qt::Key_Up:
        movingUp = false;
        break;

    case Qt::Key_Down:
        movingDown = false;
        break;
    }
}

QLabel* Robber::getScoreLabel()
{
    return scoreLabel;
}

QLabel* Robber::getPoliceNearbyLabel()
{
    return policeNearby;
}

void Robber::incrementScore(int val) {
    score+=val;
    if (score<0)
        score=0;
    QString s;
    s.setNum(score);
    s="Robber's score = "+s;
    scoreLabel->setText(s);
}

*/
