#include "Police.h"
#include "PlayWindow.h"
#include <iostream>
using namespace std;

Police::Police()
{
    setPixmap(QPixmap(string("photos/police").c_str()).scaled(30,30));

}

QRectF Police::boundingRect() const{
    return QRectF(0, 0, 30, 30);

}

//checks of circle is on map //returns true is on map
bool Police::onMap()
{
    for(unsigned int i =0; i<PlayWindow::map->mapRoads.size(); i++){
        if(collidesWithItem(PlayWindow::map->mapRoads[i],Qt::ContainsItemShape)){
            return true;
        }
    }
    return false;
}


QPointF Police::pos() const
{
    QRectF r = this->boundingRect();
    return QPointF(QGraphicsPixmapItem::pos().x()+r.width()/2,QGraphicsPixmapItem::pos().y()+r.height()/2);
}


void Police::setPos(qreal x, qreal y)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(x-r.width()/2,y-r.height()/2);
}

void Police::setPos (const QPointF & pos)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(pos.x()-r.width()/2,pos.y()-r.height()/2);
}
