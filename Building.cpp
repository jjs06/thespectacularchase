#include "Building.h"

Building::Building(string type)
{
    if (type=="0")
    {
        //HOUSE
        cashAmount=rand()%900+100;
        this->setPixmap(QPixmap(string("photos/house").c_str()).scaled(80,80));
        guardedStatus=(rand()%100>49)?0:1; //start off with residents possibly in or out of house
        guardedString = (guardedStatus)?("\nOccupants are home."):("\nOccupants not home.");

    }
    else if (type=="1")
    {
        //SHOP
        cashAmount=rand()%4000+1000;
        this->setPixmap(QPixmap(string("photos/shop").c_str()).scaled(80,80));
        guardedStatus=2; //start off with guards
        guardedString="\nGuarded.";
    }
    else if (type=="2")
    {
        //BANK
        cashAmount=rand()%10000+45000;
        this->setPixmap(QPixmap(string("photos/bank").c_str()).scaled(80,80));
        guardedStatus=2; //start off with guards
        guardedString="\nGuarded.";
    }

    buildingInfo = new QLabel("");
    QString s;
    s.setNum(cashAmount);
    buildingInfo->setText("Money = $" + s + guardedString);
    buildingInfo->setGeometry(0,0,140,40);
    buildingInfo->hide();
}

int Building::getMoney()
{
    return cashAmount;
}

int Building::getGuardedStatus()
{
    return guardedStatus;
}

void Building::setPos(qreal x, qreal y)
{
    QGraphicsPixmapItem::setPos(x,y);
    buildingInfo->setGeometry(x,y,140,40);
}

void Building::setMoney(int money)
{
    this->cashAmount = money;
    QString s;
    s.setNum(cashAmount);
    buildingInfo->setText("Money = $" + s + guardedString);
}

void Building::setGuardedStatus(int guardedStatus)
{
    this->guardedStatus = guardedStatus;
}

QPointF Building::pos() const
{
    QRectF r = this->boundingRect();
    return QPointF(QGraphicsPixmapItem::pos().x()+r.width()/2,QGraphicsPixmapItem::pos().y()+r.height()/2);
}

QLabel *Building::getBuildingInfo()
{
    return buildingInfo;
}

void Building::rob()
{
    cashAmount=0;
    buildingInfo->setText("Money = $0"+guardedString);
}

int Building::isGuarded()
{
    return guardedStatus;
}

void Building::flipGuard()
{
    if (guardedStatus == 0) {
        guardedStatus=1;
        guardedString="\nOccupants are home.";
    }
    else if (guardedStatus == 1) {
        guardedStatus=0;
        guardedString="\nOccupants not home.";
    }
    else if (guardedStatus ==2) {
        guardedStatus=3;
        guardedString="\nGuards on vacation.";
    }
    else if (guardedStatus==3) {
        guardedStatus=2;
        guardedString="\nGuarded.";
    }
    QString s;
    s.setNum(cashAmount);
    buildingInfo->setText("Money = $" + s + guardedString);
}
