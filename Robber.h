/**
\file Robber.h
\brief A Robber character whose primary task is to steal Building money and escap the Police.

Contains several 'motion' booleans for smoother movement.
Has the ability to shift the scene around him for large maps.
*/

#ifndef ROBBER_H
#define ROBBER_H

#include "QtGui"
#include "QRectF"
#include "Map.h"

class Robber:public QGraphicsPixmapItem
{

private:
    QGraphicsScene* scene;
    bool ableToSteal;
    bool movingLeft;
    bool movingRight;
    bool movingUp;
    bool movingDown;
    bool toMoveScene;
    int topX,topY;
    int speed;

public:

    /**
      Constructs a robber and sets up the necessary parameters.
      */
    Robber(bool _ableToSteal, QGraphicsScene* _scene=NULL,bool playingMode=true);

    /**
      Returns the boundingRect of the Robber.
      */
    QRectF boundingRect() const;

    /**
      Returns a boolean specifying whether the Robber has the ability to steal a Building.
      */
    bool getAbleToSteal();

    /**
      Sets the movingRight boolean to the passed argument.
      */
    void setMovingRight(bool b);
    /**
      Sets the movingLeft boolean to the passed argument.
      */
    void setMovingLeft(bool b);
    /**
      Sets the movingUp boolean to the passed argument.
      */
    void setMovingUp(bool b);
    /**
      Sets the movingDown boolean to the passed argument.
      */
    void setMovingDown(bool b);

    /**
      Returns the position of the Robber.
      */
    QPointF pos() const;

    /**
      Returns true if the Robber is on the Map (on a Road).
      Returns false otherwise.
    */
    bool onMap();

    /**
      Moves the Robber based on the 'moving' booleans.
      If mapType is 2, shifts the scene opposite to the robber to keep him in perspective while the map shifts.
      */
    void update();


    /**
      Sets the position of the Robber given the x and y values.
    */
    void setPos(qreal x, qreal y);

    /**
      Sets the position of the Robber given a QPointF.
    */
    void setPos(const QPointF & pos);
};

#endif // ROBBER_H
