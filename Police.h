/**
\file Police.h
\brief A Police character whose primary task is to catch the Robber.

*/

#ifndef POLICE_H
#define POLICE_H

#include "QtGui"
#include "QRectF"
#include "Map.h"

class Police: public QObject, public QGraphicsPixmapItem  //QObject needed for slots and signals
{
    Q_OBJECT

public:
    Police();

    /**
    The bounding rectangle of the CircleTri. It is used for collision.
    It is the bounding rectangle of the Circle.
    */
    QRectF boundingRect() const;

    /**
    If Police is on the map (on the road) it returns true, else it returns false
    */
    bool onMap();

    QPointF pos() const;
    /**
      Sets the position of the Police given the x and y values.
    */
    void setPos(qreal x, qreal y);

    /**
      Sets the position of the Police given a QPointF.
    */
    void setPos(const QPointF & pos);


};

#endif // POLICE_H
