#include "DbHandler.h"

QSqlDatabase DbHandler::db;

void DbHandler::open()
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    bool ok = db.open();
    if (!ok)
        cout << "Failed to connect to root mysql admin";

    QSqlQuery qu(db.database());
    qu.prepare("CREATE DATABASE IF NOT EXISTS TheSpectacularChase");
    qu.exec();
    db.setDatabaseName("TheSpectacularChase");
    db.close();
    db.open();

    qu.prepare("CREATE TABLE IF NOT EXISTS `Users` (`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY, `username` varchar(255) NOT NULL, `highscore` int NOT NULL);");
    qu.exec();
    qu.finish();
    qu.prepare("CREATE TABLE IF NOT EXISTS `Saved` (`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY, `savedname` varchar(255) NOT NULL, `userId` int NOT NULL, `mapType` int NOT NULL, `playingmode` boolean NOT NULL, `score` int NOT NULL, `robberscore` int NOT NULL, `moneystolen` int NOT NULL, `gametime` int NOT NULL, `robberPosX` float NOT NULL, `robberPosY` float NOT NULL, dateCreated datetime NOT NULL, FOREIGN KEY (userId) REFERENCES Users(id) ON DELETE CASCADE);");
    qu.exec();
    qu.finish();
    qu.prepare("CREATE TABLE IF NOT EXISTS `Police` (`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY, `savedId` int NOT NULL, `posX` float NOT NULL, `posY` float NOT NULL, FOREIGN KEY (savedId) REFERENCES Saved(id) ON DELETE CASCADE);");
    qu.exec();
    qu.finish();
    qu.prepare("CREATE TABLE IF NOT EXISTS `Buildings` (`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY, `savedId` int NOT NULL, money int NOT NULL, guarded int NOT NULL, FOREIGN KEY (savedId) REFERENCES Saved(id) ON DELETE CASCADE);");
    qu.exec();
    qu.finish();

    addUsername("Default");
}

void DbHandler::addUsername(QString username)   //sets highscore to 0
{
    QSqlQuery qu(db.database());
    qu.prepare("SELECT * FROM Users WHERE username=:username;");
    qu.bindValue(":username",username);
    qu.exec();
    if(!qu.next())
    {
        qu.prepare("INSERT INTO Users VALUES (NULL, :username, :highscore);");
        qu.bindValue(":username",username);
        qu.bindValue(":highscore",0);
        qu.exec();
    }
}

void DbHandler::updateHighScore(QString username, int highscore)
{
    QSqlQuery qu(db.database());
    qu.prepare("UPDATE Users SET highscore=:highscore WHERE username=:username;");
    qu.bindValue(":highscore",highscore);
    qu.bindValue(":username",username);
    qu.exec();
}

int DbHandler::retrieveScore(QString username)
{
    QSqlQuery qu(db.database());
    qu.prepare("SELECT highscore FROM Users WHERE username=:username;");
    qu.bindValue(":username",username);
    qu.exec();
    qu.next();
    return qu.value(0).toInt();
}

vector<QString>* DbHandler::retrieveUsernames()
{
    vector<QString> *ret = new vector<QString>;
    QSqlQuery q(db.database());
    q.prepare("SELECT username FROM Users WHERE username != 'Default' ORDER BY username;");
    q.exec();
    while (q.next())
    {
        ret->push_back(QString(q.value(0).toString()));
    }
    return ret;
}

void DbHandler::save(PlayWindow *game, QString savedname)
{
    QSqlQuery q(db.database());

    q.prepare("SELECT id FROM Users WHERE username = :username;");
    q.bindValue(":username", game->getUsername());
    q.exec();
    q.next();
    int userId = q.value(0).toInt();
    q.finish();

    //delete duplicate (overwrite)
    q.prepare("DELETE FROM Saved WHERE savedname = :savedname AND userId = :userId;");
    q.bindValue(":savedname", savedname);
    q.bindValue(":usedId", userId);
    q.exec();
    q.finish();

    q.prepare("INSERT INTO Saved (savedname, userId, mapType, playingmode, score, robberscore, moneystolen, gametime, robberPosX, robberPosY, dateCreated) VALUES (:savedname, :userId, :mapType, :playingmode, :score, :robberscore, :moneystolen, :gametime, :robberPosX, :robberPosY, NOW());");
    q.bindValue(":savedname", savedname);
    q.bindValue(":userId", userId);
    q.bindValue(":mapType", game->map->getType());
    q.bindValue(":playingmode", game->getPlayingMode());
    q.bindValue(":score", game->getScore());
    q.bindValue(":robberscore", game->robberScore);
    q.bindValue(":moneystolen", game->getMoneyStolen());
    q.bindValue(":gametime", (int)(time(0) - game->getTime0()));
    q.bindValue(":robberPosX", game->getRobber()->pos().x());
    q.bindValue(":robberPosY", game->getRobber()->pos().y());
    q.exec();
    q.finish();

    q.prepare("SELECT id FROM Saved WHERE savedname = :savedname;");
    q.bindValue(":savedname",savedname);
    q.exec();
    q.next();
    int savedId = q.value(0).toInt();
    q.finish();

    for(unsigned int i=0; i < game->getPoliceDep()->getSize(); i++)
    {
        Police* p = game->getPoliceDep()->getPolice(i);
        q.prepare("INSERT INTO Police (savedId, posX, posY) VALUES (:savedId, :posX, :posY);");
        q.bindValue(":savedId", savedId);
        q.bindValue(":posX", p->pos().x());
        q.bindValue(":posY", p->pos().y());
        q.exec();
        q.finish();
    }

    for(unsigned int i=0; i < PlayWindow::map->mapBuildings.size(); i++)
    {
        Building* b = PlayWindow::map->mapBuildings[i];
        q.prepare("INSERT INTO Buildings (savedId, money, guarded) VALUES (:savedId, :money, :guarded);");
        q.bindValue(":savedId", savedId);
        q.bindValue(":money", b->getMoney());
        q.bindValue(":guarded",b->getGuardedStatus());
        q.exec();
        q.finish();
    }
}

vector<QString>* DbHandler::retrieveSavedGames(QString username)
{
    vector<QString> *ret = new vector<QString>;
    QSqlQuery q(db.database());

    q.prepare("SELECT id FROM Users WHERE username = :username;");
    q.bindValue(":username", username);
    q.exec();
    q.next();
    int userId = q.value(0).toInt();
    q.finish();

    q.prepare("SELECT savedname FROM Saved WHERE userId = :userId ORDER BY dateCreated;");
    q.bindValue(":userId", userId);
    q.exec();
    while (q.next())
    {
        ret->push_back(QString(q.value(0).toString()));
    }
    return ret;
}

PlayWindow* DbHandler::load(QString savedname, QString username)
{
    PlayWindow* pw;
    QSqlQuery q(db.database());

    q.prepare("SELECT id FROM Users WHERE username = :username;");
    q.bindValue(":username", username);
    q.exec();
    q.next();
    int userId = q.value(0).toInt();
    q.finish();

    q.prepare("SELECT * FROM Saved WHERE savedname = :savedname AND userId = :userId;");
    q.bindValue(":savedname", savedname);
    q.bindValue(":userId", userId);
    q.exec();
    q.next();

    int savedId = q.value(0).toInt();
    int mapType = q.value(3).toInt();
    bool playingmode = q.value(4).toBool();
    int score = q.value(5).toInt();
    int robberscore=q.value(6).toInt();
    int moneystolen = q.value(7).toInt();
    int gametime = q.value(8).toInt();
    QPointF robberPos;
    robberPos.setX(q.value(9).toDouble());
    robberPos.setY(q.value(10).toDouble());
    q.finish();

    int highscore = retrieveScore(username);

    pw = new PlayWindow(username, highscore, mapType, playingmode);

    pw->getRobber()->setPos(robberPos);
    pw->setScore(score);
    pw->decrementTime0(gametime);
    pw->robberScore=robberscore;
    pw->setMoneyStolen(moneystolen);


    q.prepare("SELECT * FROM Police WHERE savedId = :savedId;");
    q.bindValue(":savedId", savedId);
    q.exec();
    int i=0;
    while(q.next())
    {
        QPointF policePos;
        policePos.setX(q.value(2).toDouble());
        policePos.setY(q.value(3).toDouble());
        pw->getPoliceDep()->getPolice(i)->setPos(policePos);
        pw->getPoliceDep()->setLoc(i, policePos);
        i++;
    }
    q.finish();

    q.prepare("SELECT * FROM Buildings WHERE savedId = :savedId;");
    q.bindValue(":savedId", savedId);
    q.exec();
    i=0;
    while(q.next())
    {
        PlayWindow::map->mapBuildings[i]->setMoney(q.value(2).toInt());
        PlayWindow::map->mapBuildings[i]->setGuardedStatus(q.value(3).toInt());
        i++;
    }
    q.finish();
    return pw;
}


void DbHandler::close()
{
   db.close();
}
