#include "PoliceDep.h"
#include <iostream>

PoliceDep::PoliceDep()
{
    tPressed=false;
    specialRemaining=3;
}

void PoliceDep::addPolice(Police *p)
{
    policeMen.push_back(p);
    policePos.push_back(p->pos());
}

void PoliceDep::setLoc(int i, QPointF loc) {
    policePos[i]=loc;
}

Police* PoliceDep::getPolice(unsigned int pos)
{
    return policeMen[pos];
}

QPointF PoliceDep::getLoc(unsigned int i)
{
    return policePos[i];
}

unsigned int PoliceDep::getSize()
{
    return policeMen.size();
}

void PoliceDep::addPoliceToScene(QGraphicsScene* scene)
{
    for(unsigned int i=0; i<policeMen.size(); i++)
    {
        scene->addItem(policeMen[i]);
    }
}
