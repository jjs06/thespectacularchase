#include "Flag.h"
#include <string>
using namespace std;

Flag::Flag()
{
    this->setPixmap(QPixmap(string("photos/flag").c_str()).scaled(30,30));
    this->setFlag(QGraphicsItem::ItemIsMovable);
}

QPointF Flag::pos() const
{
    QRectF r = this->boundingRect();
    return QPointF(QGraphicsPixmapItem::pos().x()+r.width()/2,QGraphicsPixmapItem::pos().y()+r.height()/2);
}

void Flag::setPos(qreal x, qreal y)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(x-r.width()/2,y-r.height()/2);
}

void Flag::setPos (const QPointF & pos)
{
    QRectF r = this->boundingRect();
    QGraphicsItem::setPos(pos.x()-r.width()/2,pos.y()-r.height()/2);
}

