/**
\file Building.h
\brief A Building with several types.

Can be a house, bank, or shop. Each has a specific monetary value and guarded status.
*/

#ifndef BUILDING_H
#define BUILDING_H

#include "QtGui"
#include <string>
using namespace std;

class Building: public QGraphicsPixmapItem
{
private:
    int cashAmount;
    int guardedStatus; //0 = not guarded; 1 = residents in house; 2 = has guards present;
                        //3 = has guards on vacation
    QString guardedString;
    QLabel* buildingInfo;
public:
    /**
      Constructs a Building.
      Type 0 constructs a house, type 1 constructs a shop, type 2 constructs a bank.
      */
    Building(string type);

    /**
      Retrieve amount of money in the Building.
    */
    int getMoney();
    /**
      Retrieve guarded status of the Building:
      0 = not guarded
      1 = residents in house
      2 = has guards present;
      3 = has guards on vacation
    */
    int getGuardedStatus();
    /**
      Retrieve all information regarding the Building in a QLabel displayed near the Building.
    */
    QLabel* getBuildingInfo();

    /**
      Sets the position of the Building and its information (QLabel).
    */
    void setPos(qreal x,qreal y);

    /**
      Sets the money of the Building.
    */
    void setMoney(int money);

    /**
      Sets the guarded status of the Building.
    */
    void setGuardedStatus(int guardedStatus);
    /**
      Retrieve the position of the Building.
    */
    QPointF pos() const;
    /**
      The Building is robbed, the cashAmount is set to $0
    */
    void rob();

    /**
    Returns the guarded status.
    */
    int isGuarded();

    /**
      Flips the status of the Building from guarded to unguarded (or vice-versa).
      If the Building is a house, it is flipped from residents at home to residents not home (or vice-versa)
    */
    void flipGuard();
};

#endif // BUILDING_H
