/**
\file PlayWindow.h
\brief The main window where the game takes place.

Contains functions for handling Robber and Police movements, user input, and end-of-game functionality.
*/

#ifndef PLAYWINDOW_H
#define PLAYWINDOW_H

#include <QtGui>
#include <iostream>
using namespace std;

#include "Map.h"
#include "Robber.h"
#include "PoliceDep.h"
#include "AbstractMap.h"
#include "Functions.h"

const QString day[7] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

class PlayWindow: public QGraphicsView
{
    Q_OBJECT

private:

    QGraphicsScene* scene;

    QString username;
    int highscore;
    bool playingMode;
    bool gameOver;
    bool won;
    int moneyStolen;
    int score;
    int topRight;
    int topLeft;

    PoliceDep* policeDep;
    unsigned int selectedPolice;
    Robber* robber;
    time_t t0;
    time_t pauseTime;
    bool isWeekend;
    QGraphicsPixmapItem* warningCircle;
    QTimer timer;
    QTimer circleTimer;

    int movedCount;

    QGraphicsProxyWidget* pauseProxyWidget;
    QVBoxLayout* pauseMenuLayout;
    QVBoxLayout* gameOverLayout;
    QPushButton* resumeGamePB;
    QPushButton* saveGamePB;
    QPushButton* mainMenuPB;
    QPushButton* exitPB;
    QPushButton* retryPB;
    QPushButton* gameOverPB;

public:
    static int robberScore;
    static QLabel* scoreLabel;
    static QLabel* policeNearbyLabel;
    static QLabel* robberNearbyLabel;
    static QLabel* targetScoreLabel;
    static QLabel* moneyStolenLabel;
    static QLabel* timeLabel;
    static QWidget* pauseMenu;
    static QWidget* gameOverMenu;
    static Map* map;
    static AbstractMap* am;

    /**
      Constructs a scene with a map, robber, and police deparment.
      */
    PlayWindow(QString user, int high, int mapType, bool mode);

    /**
      Returns the playingMode.
      playingMode=1 means the user is playing as the Police.
      playingMode=0 means the user is playing as the Robber.
      */
    bool getPlayingMode();

    /**
      Returns the username the user is playing as.
      */
    QString getUsername();

    /**
      Retrieves time0.
      */
    time_t getTime0();

    /**
      Retrieves the Robber.
      */
    Robber* getRobber();

    /**
      Retrieves the PoliceDep.
      */
    PoliceDep* getPoliceDep();

    /**
      Retrieves the player's current score.
      */
    int getScore();

    /**
      Retrieves the money stolen so far by the Robber.
      */
    int getMoneyStolen();

    /**
      Sets the player's score to the given value.
      Usually used when loading games.
      */
    void setScore(int score);

    /**
      Sets the money stolen amount to the passed value.
      Usually used when loading games.
      */
    void setMoneyStolen(int val);
    /**
      Acts according to input from the mouse.
      Drags flags across the map if left-clicking.
      Moves the selected Police to the location of the cursor if right-clicking and playing as Police.
      */
    void mousePressEvent(QMouseEvent *event);

    /**
      Responds to keys input.
      Moves the scene if arrow keys are input and playing as the Police.
      Selects a Police if input was numbers and playing as the Police.
      Moves the Robber if input was arrow keys and playing as the Robber.
            Steals a nearby building if possible, if the input was the space and playing as the Robber.
      Drops a flag if input was F.
      Pauses the game if the input was ESC.
      */
    void keyPressEvent(QKeyEvent *event);

    /**
      Updates the necessary Robber 'moving' booleans if the input is the arrow keys.
      */
    void keyReleaseEvent(QKeyEvent *event);

    /**
      Increments the score and moneyStolen by val and updates the labels.
      */
    void incrementScore(int val);

    /**
      Decrements the value of time0 by amount(Used when Loading a Saved game)
    */
    void decrementTime0(int amount);

    /**
      Displays the information of nearby buildings according to the playingMode:
      -if the player is the Robber, only display neaby buildings (<60 ~ 120 meters)
      -if the player is the Police, display all information for all buildings
    */
    void displayNearbyBuildings();

    pair<double,unsigned int> findNearestPolice(QPointF loc);
    /**
    A function to move the opponent (opponent specified according to playingMode)
    The movement of the opponent is done according to the Artificial Intelligence implemented in AbstractMap
    */
    void moveOpponent();
    /**
    Sets the tracking location of all police members to the robber's position.
    */
    void allFollowRobber(QPointF loc);

    /**
      Move all police department members to their wanted locations by using the shortest path, whether playing as Police or Robber.
    */
    void movePoliceDep();

    /**
    Move the Robber automatically by calling AbstractMap's Dijkstra function.
    */
    void moveRobber();

    /**
      Add all new Flags to the Map.
    */
    void addNewMapFlags();

    /**
      Updates the game time and its label.
      */
    void updateTime();

    /**
      Shows the game over screen and stops the game if the user lost or won.
      */
    void showGameOverScreen(bool _won);

public slots:
    /**
      Updates the state of the game by calling moveOpponent, displayNearbyBuildings, and adjusting the time and day.
    */
    void update();

    /**
      Re-sets the PlayWindow.
      */
    void retry();

    /**
      Hides the warning circle given to the Police when the robber steals a Building with house residents or a Building with guards in the neighborhood.
    */
    void hideCircle();

    /**
      Resumes the game from the pause screen.
      */
    void resumeGame();

    /**
      Saves the game.
      Interacts with DbHandler.
      */
    void saveGame();

    /**
      Closes the game and opens the MainMenu.
      */
    void mainMenu();

    /**
      Closes the game.
      */
    void exit();
};

#endif // PLAYWINDOW_H
