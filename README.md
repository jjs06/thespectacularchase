# Welcome to our awesome game!

The master and updated branches contain the same code (though different histories), so feel free to clone any of them. They should work without fault when cloned to Ubuntu and run from the included executable.

Enjoy! 

## Known issues

- Police right-click is faulty sometimes, you need to click several times to get the selected policeman to move.
- On the "pause"/"game over" menus, you may need to double click an item to select it.
- There is an unknown bug where the game crashes/freezes unexpectedly, though this is rare.

## Details

This game was created from scratch using Qt and SQL for EECE 435L, a software tools laboratory at AUB Lebanon, over the course of 3 months, by Jimmy Saade and Samer Bekhazi. 

## Game Logistics

### Starting the game
Go to Select Account in order to create a new account or to choose an account. A Default
account is included to automatically save your progress even if you fail to create an account.

- Go to Instructions to check the detailed instructions in the game play
- Go to Load Game if you want to load a previously save game
- Go to Select City in order to choose a city from Beirut, Zahle, and Nabatieh
- Under New Game, select a playing mode: play as robber, or as police
- Press New Game, and wait for the game to load!

While in the game, press ESC to pause and display the pause menu. In this menu, you can save
your progress by pressing Save Game, quit the current game by pressing Quit Game (redirects to
the main menu), exit the game by pressing Exit, or simply Resume Game (you can also press
ESC again to resume the game).

### Game Rules
- The initial score of the robber is zero
- The initial score of the police is the sum of the money in all buildings in the city
- Every second, the robbers’ score and the polices’ score decrease by 5 points
- The robbers’ score and polices’ score never reach negative values (Minimum score is 0)
- Every time the robber steals a building, the money in this building is added to his score,
and removed from the polices’ score
- The robber can only steal a building once
- If the robber steals a guarded building, the guard catches him, and the police wins
- If the robber steals a building near another guarded building (within 200 px), the guard
calls the police and notifies them of the region of the robbery (in police mode, a circle on
the map shows the approximate region. In robber mode, one policeman comes to the region
to check it out)
- If the robber steals a house and the residents are there, they call the police and notify them
of the region of the robbery (as above)
- When a police is near the robber, the robber can see him on the map, and gets notified
- When a robber is near the police, the police can see him on the map, and gets notified. In
robber mode, if one policeman can see the robber, he immediately notifies all other
policemen and they all follow the robber
- If a police catches the robber, the police wins
- If the robber steals more than the required money (or as much), and escapes through one of
the gates safely, the robber wins

### Player Movement
Playing as a robber:

- Use the arrows to move the robber
- Use the Space key to steal a building
- Use the F key to add a flag, and the F key again to remove the flag while standing on top
of it

Playing as the police:

- Select a police using the Numbers (1 – 9, or 0 to select the 10th). When there are less
than 10 policemen, the higher numbers do nothing
- Move the selected police by specifying the target position using the right-click
- Press the T key and right-click to simultaneously move all police (you can do this 3
times per game only)
- Use the F key to add a flag, and the F key again to remove the flag while standing on top
of it