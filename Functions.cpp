#include "Functions.h"

double Functions::calculateDistance(QPointF point1, QPointF point2)
{
    int x1 = point1.x();
    int x2 = point2.x();
    int y1 = point1.y();
    int y2 = point2.y();
    return sqrt((double)((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));

}

double Functions::calculateManhattanDistance(QPointF point1, QPointF point2)
{
    int x1 = point1.x();
    int x2 = point2.x();
    int y1 = point1.y();
    int y2 = point2.y();
    return (abs(x1-x2)+abs(y1-y2)); //Manhattan distance
}
