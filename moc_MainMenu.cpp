/****************************************************************************
** Meta object code from reading C++ file 'MainMenu.h'
**
** Created: Tue May 6 03:32:13 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MainMenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainMenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,
      20,    9,    9,    9, 0x0a,
      31,    9,    9,    9, 0x0a,
      38,    9,    9,    9, 0x0a,
      45,    9,    9,    9, 0x0a,
      52,    9,    9,    9, 0x0a,
      59,    9,    9,    9, 0x0a,
      66,    9,    9,    9, 0x0a,
      85,    9,    9,    9, 0x0a,
     104,    9,    9,    9, 0x0a,
     128,    9,    9,    9, 0x0a,
     158,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainMenu[] = {
    "MainMenu\0\0newGame()\0loadGame()\0inst()\0"
    "city()\0exit()\0back()\0acct()\0"
    "loadSelectedGame()\0updateMapType(int)\0"
    "updateUsername(QString)\0"
    "updateLoadGameString(QString)\0addUser()\0"
};

void MainMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainMenu *_t = static_cast<MainMenu *>(_o);
        switch (_id) {
        case 0: _t->newGame(); break;
        case 1: _t->loadGame(); break;
        case 2: _t->inst(); break;
        case 3: _t->city(); break;
        case 4: _t->exit(); break;
        case 5: _t->back(); break;
        case 6: _t->acct(); break;
        case 7: _t->loadSelectedGame(); break;
        case 8: _t->updateMapType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->updateUsername((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->updateLoadGameString((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->addUser(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainMenu::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MainMenu,
      qt_meta_data_MainMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainMenu))
        return static_cast<void*>(const_cast< MainMenu*>(this));
    return QObject::qt_metacast(_clname);
}

int MainMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
