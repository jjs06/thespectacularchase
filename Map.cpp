#include "Map.h"
#include "Functions.h"
#include <math.h>
#include <iostream>

int Map::winScore;

Map::Map(int type, vector<Road*> MapRoads, vector<Building*> MapBuildings, vector<Gate*> MapGates)
{
    sum=0;
    this->type = type;
    mapRoads = MapRoads;
    mapBuildings = MapBuildings;
    mapGates = MapGates;
    buildingEntrances.resize(mapBuildings.size(),QPointF(-50,-50));
    gateEntrances.resize(mapGates.size(),QPointF(-50,-50));
    for (unsigned int i=0; i<mapBuildings.size(); i++) {
        sum+=mapBuildings[i]->getMoney();
    }
}

int Map::getType()
{
    return type;
}

pair<int,int> Map::tryToSteal(QPointF loc)
{
    for (unsigned int i=0; i<mapBuildings.size(); i++)
    {
        if (Functions::calculateDistance(loc,mapBuildings[i]->pos())<65)
        {
            int money = mapBuildings[i]->getMoney();
            if (money==0) continue;
            mapBuildings[i]->rob();
            int x=mapBuildings[i]->isGuarded();
            if (x==1 || x==2)
            {
                return make_pair(money,x); //if 1 or 2, 1=ppl home, 2=guarded
            }
            else {
                bool g = guardNearby(loc);
                x = g?4:0;
                return make_pair(money,x); //0 if safe, 4 if guard in neighbourhood
            }
        }
    }
    return make_pair(0,false);
}

bool Map::tryToWin(QPointF loc, int score) {
    for (unsigned int i=0; i<mapGates.size(); i++){
        if (score > winScore && Functions::calculateDistance(loc,mapGates[i]->pos())<55) {
            return true;
        }
    }
    return false;
}

int Map::sumAll()
{
    return sum;
}

bool Map::guardNearby(QPointF loc)
{
    for (unsigned int i=0; i<mapBuildings.size(); i++) {
        if (Functions::calculateDistance(loc,mapBuildings[i]->pos())<200 && mapBuildings[i]->isGuarded()==2)
            return true;
    }
    return false;
}

void Map::flipGuards()
{
    for (unsigned int i =0; i<mapBuildings.size(); i++)
    {
        mapBuildings[i]->flipGuard();
    }
}

void Map::setFlag(qreal x, qreal y)
{
    Flag* flag = new Flag();
    flag->setPos(x,y);
    mapFlags.push_back(flag);
}

Map* Map::constructMap(int mapType)
{
    if(mapType==0)
    {
        winScore = 80000;
        vector<Road*> MapRoads;
        Road* E1= new Road(QPoint(0,0), QPoint(600,0),50);
        Road* E2= new Road(QPoint(0,150), QPoint(600,150),50);
        Road* E3= new Road(QPoint(0,300), QPoint(600,300),50);
        Road* E4= new Road(QPoint(0,450), QPoint(600,450),50);
        Road* E5= new Road(QPoint(0,600), QPoint(600,600),50);
        Road* E6= new Road(QPoint(0,0), QPoint(0,600),50);
        Road* E7= new Road(QPoint(150,0), QPoint(150,600),50);
        Road* E8= new Road(QPoint(300,0), QPoint(300,600),50);
        Road* E9= new Road(QPoint(450,0), QPoint(450,600),50);
        Road* E10= new Road(QPoint(600,0), QPoint(600,600),50);
        MapRoads.push_back(E1);
        MapRoads.push_back(E2);
        MapRoads.push_back(E3);
        MapRoads.push_back(E4);
        MapRoads.push_back(E5);
        MapRoads.push_back(E6);
        MapRoads.push_back(E7);
        MapRoads.push_back(E8);
        MapRoads.push_back(E9);
        MapRoads.push_back(E10);

        vector<Building*> vBuildings;
        Building* b1 = new Building("1");
        Building* b2 = new Building("2");
        Building* b3 = new Building("2");
        Building* b4 = new Building("1");
        Building* b5 = new Building("0");
        Building* b6 = new Building("0");

        b1->setPos(25,195);
        b2->setPos(495,195);
        b3->setPos(25,495);
        b4->setPos(495,495);
        b5->setPos(173,495);
        b6->setPos(495,345);

        vBuildings.push_back(b1);
        vBuildings.push_back(b2);
        vBuildings.push_back(b3);
        vBuildings.push_back(b4);
        vBuildings.push_back(b5);
        vBuildings.push_back(b6);

        vector<Gate*> gates;
        Gate* g1 = new Gate();
        Gate* g2 = new Gate();
        Gate* g3 = new Gate();
        Gate* g4 = new Gate();
        Gate* g5 = new Gate();

        g1->setPos(595,-50);
        g2->setPos(445,-50);
        g3->setPos(295,-50);
        g4->setPos(155,-50);
        g5->setPos(5,-50);

        gates.push_back(g1);
        gates.push_back(g2);
        gates.push_back(g3);
        gates.push_back(g4);
        gates.push_back(g5);

        Map* aMap= new Map(mapType, MapRoads,vBuildings,gates);
        return aMap;
    }
    else if(mapType==1)
    {
        winScore=50000;
        vector<Road*> MapRoads;
        Road* E1= new Road(QPoint(0,0), QPoint(600,0),50);
        Road* E2= new Road(QPoint(0,300), QPoint(600,300),50);
        Road* E3= new Road(QPoint(0,600), QPoint(600,600),50);
        Road* E4= new Road(QPoint(0,0), QPoint(0,600),50);
        Road* E5= new Road(QPoint(300,0), QPoint(300,600),50);
        Road* E6= new Road(QPoint(600,0), QPoint(600,600),50);
        MapRoads.push_back(E1);
        MapRoads.push_back(E2);
        MapRoads.push_back(E3);
        MapRoads.push_back(E4);
        MapRoads.push_back(E5);
        MapRoads.push_back(E6);


        vector<Building*> vBuildings;
        Building* b1 = new Building("1");
        b1->setPos(25,195);
        vBuildings.push_back(b1);
        Building* b2 = new Building("2");
        b2->setPos(495,195);
        vBuildings.push_back(b2);

        vector<Gate*> gates;

        //Gates
        Gate* g1 = new Gate();
        Gate* g2 = new Gate();
        Gate* g3 = new Gate();
        Gate* g4 = new Gate();
        Gate* g5 = new Gate();

        g1->setPos(595,-50);
        g2->setPos(445,-50);
        g3->setPos(295,-50);
        g4->setPos(155,-50);
        g5->setPos(5,-50);

        gates.push_back(g1);
        gates.push_back(g2);
        gates.push_back(g3);
        gates.push_back(g4);
        gates.push_back(g5);

        Map* aMap= new Map(mapType, MapRoads,vBuildings,gates);
        return aMap;
    }
    else if (mapType==2){

        winScore=100000;
        vector<Road*> MapRoads;
        Road* E1= new Road(QPoint(600,110), QPoint(600,990),70);
        Road* E2= new Road(QPoint(600,300), QPoint(730,300),50);
        Road* E3= new Road(QPoint(600,100), QPoint(830,100),50);
        Road* E4= new Road(QPoint(830,0), QPoint(830,100),50);
        Road* E5= new Road(QPoint(200,200), QPoint(600,200),50);
        Road* E6= new Road(QPoint(200,200), QPoint(200,400),50);
        Road* E7= new Road(QPoint(0,400),QPoint(400,400),50);
        Road* E8= new Road(QPoint(400,400),QPoint(400,600),50);
        Road* E9= new Road(QPoint(400,600),QPoint(600,600),50);
        Road* E10= new Road(QPoint(0,900),QPoint(600,900),50);
        Road* E11= new Road(QPoint(400,900),QPoint(400,1200),50);
        Road* E12= new Road(QPoint(600,900),QPoint(1000,900),50);
        Road* E13= new Road(QPoint(1000,600),QPoint(1000,900),50);
        Road* E14= new Road(QPoint(730,600),QPoint(1000,600),50);
        Road* E15= new Road(QPoint(730,300),QPoint(730,600),50);
        Road* E16= new Road(QPoint(0,400),QPoint(0,900),50);
        MapRoads.push_back(E2);
        MapRoads.push_back(E3);
        MapRoads.push_back(E4);
        MapRoads.push_back(E5);
        MapRoads.push_back(E6);
        MapRoads.push_back(E7);
        MapRoads.push_back(E8);
        MapRoads.push_back(E9);
        MapRoads.push_back(E10);
        MapRoads.push_back(E11);
        MapRoads.push_back(E12);
        MapRoads.push_back(E13);
        MapRoads.push_back(E14);
        MapRoads.push_back(E15);
        MapRoads.push_back(E16);
        MapRoads.push_back(E1);

        vector<Building*> vBuildings;
        Building* b1 = new Building("1");
        Building* b2 = new Building("2");
        Building* b3 = new Building("2");
        Building* b4 = new Building("1");
        Building* b5 = new Building("0");
        Building* b6 = new Building("0");
        Building* b7 = new Building("2");
        Building* b8 = new Building("1");
        Building* b9 = new Building("0");

        b1->setPos(165,97);
        b2->setPos(495,100);
        b3->setPos(635,200);
        b4->setPos(275,795);
        b5->setPos(900,795);
        b6->setPos(635,795);
        b7->setPos(460,500);
        b8->setPos(75,795);
        b9->setPos(475,795);

        vBuildings.push_back(b1);
        vBuildings.push_back(b2);
        vBuildings.push_back(b3);
        vBuildings.push_back(b4);
        vBuildings.push_back(b5);
        vBuildings.push_back(b6);
        vBuildings.push_back(b7);
        vBuildings.push_back(b8);
        vBuildings.push_back(b9);

        vector<Gate*> gates;

        //Gates
        Gate* g1 = new Gate();
        Gate* g2 = new Gate();
        Gate* g3 = new Gate();
        Gate* g4 = new Gate();
        Gate* g5 = new Gate();
        g1->setPos(830,-50);
        g2->setPos(0,350);
        g3->setPos(1000,550);
        g4->setPos(400,350);
        g5->setPos(400,150);

        gates.push_back(g1);
        gates.push_back(g2);
        gates.push_back(g3);
        gates.push_back(g4);
        gates.push_back(g5);

        Map* aMap= new Map(mapType, MapRoads,vBuildings,gates);
        return aMap;
    }
    return NULL;
}
