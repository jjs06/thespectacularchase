#include "Road.h"

Road::Road(QPoint startPoint, QPoint endPoint, int width)
    :QGraphicsRectItem(startPoint.x()-width/2,startPoint.y()-width/2,endPoint.x()-startPoint.x()+width,endPoint.y()-startPoint.y()+width)
{
    this->startPoint = startPoint;
    this->endPoint = endPoint;
    this->width = width;
}

//note that the road item can be any other simple item as line or rectangle
//this is just an illustration of how one can paint an item
void Road::paint(QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setPen(Qt::transparent);
    painter->setBrush(QColor(100,100,100));
    painter->drawRect(startPoint.x()-width/2,startPoint.y()-width/2 ,endPoint.x()-startPoint.x()+width,endPoint.y()-startPoint.y()+width);

    int lineL=20;
    int lineW=4;
    int dist=4;
    painter->setBrush(Qt::white);
    for(qreal x = startPoint.x(); x + lineL < endPoint.x(); x += (lineL+dist) )
        painter->drawRect(x-lineW/2,startPoint.y()-lineW/2,lineL, lineW);
    for(qreal y = startPoint.y(); y + lineL < endPoint.y(); y += (lineL+dist) )
        painter->drawRect(startPoint.x()-lineW/2,y-lineW/2,lineW, lineL);
}

/*
QRectF Road::boundingRect() const
{
    return QRectF(1,1,1,1);
}
*/
