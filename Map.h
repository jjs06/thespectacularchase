/**
\file Map.h
\brief A map consisting of multiple roads (Road) and buildings (Building)

Contains a vector of mapRoads that defines the map shape, as well as a vector of mapBuildings that populate the map
*/

#ifndef MAP_H
#define MAP_H

#include "Road.h"
#include "Building.h"
#include "Gate.h"
#include "Flag.h"
#include "QtGui"
#include <vector>
using namespace std;

class Map
{
private:
    int type;
    int sum;
public:
    static int winScore;

    /**
      Constructs a map of the given type.
      */
    Map(int type, vector<Road*> MapRoads, vector<Building*> MapBuildings, vector<Gate *> MapGates);

    /**
      Return an integer specifying the type of the map.
    */
    int getType();
    /**
      Checks for Buildings near the location loc (QPointF), and steals them if close enough (<60 ~ 120 meters). Returns true if the building is guarded.
    */
    pair<int,int> tryToSteal(QPointF loc);

    /**
      The Robber at location loc tries to get out a gate with his current score.
    */
    bool tryToWin(QPointF loc, int score);

    /**
      Returns the sum of all the money in all Buildings on the Map.
    */
    int sumAll();

    /**
      Calls flipGuard for all buildings in the Map.
    */
    void flipGuards();

    /**
      Sets a flag on the map in the location (x,y)
    */
    void setFlag(qreal x, qreal y);

    /**
      Checks if there is a guard in the neighbourhood of loc.
    */
    bool guardNearby(QPointF loc);

    /**
      Constructs one of the pre-defined Maps depending on the argument.
    */
    static Map* constructMap(int mapType);

    vector<Road*> mapRoads;
    vector<Building*> mapBuildings;
    vector<Gate*> mapGates;
    vector<Flag*> mapFlags;
    vector<QPointF> buildingEntrances;
    vector<QPointF> gateEntrances;
};

#endif // MAP_H
