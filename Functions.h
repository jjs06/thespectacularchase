/**
\file Functions.h
\brief A class with some supplementary functions.

*/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QtGui>

class Functions
{
public:
    static int score;
    /**
    A static function to calculate the geometric distance between two points (QPointF).
    */
    static double calculateDistance(QPointF point1, QPointF point2);
    /**
    A static function to calculate the Manhattan distance between two points (QPointF).
    */
    static double calculateManhattanDistance(QPointF point1, QPointF point2);
};

#endif // FUNCTIONS_H
