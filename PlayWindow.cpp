#include "PlayWindow.h"
#include "DbHandler.h"
#include "MainMenu.h"

Map* PlayWindow::map;
AbstractMap* PlayWindow::am;
int PlayWindow::robberScore;
QWidget* PlayWindow::pauseMenu;
QWidget* PlayWindow::gameOverMenu;
QLabel* PlayWindow::scoreLabel;
QLabel* PlayWindow::policeNearbyLabel;
QLabel* PlayWindow::robberNearbyLabel;
QLabel* PlayWindow::targetScoreLabel;
QLabel* PlayWindow::moneyStolenLabel;
QLabel* PlayWindow::timeLabel;

PlayWindow::PlayWindow(QString user, int high, int mapType, bool mode)
{
    if (mapType==2)
        topRight=250;
    else topRight=-50;
    topLeft=-50;

    srand(time(0));
    scene= new QGraphicsScene;
    scene->setSceneRect(topRight, topLeft, 700, 700);

    gameOver=false;

    pauseMenu = new QWidget;

    resumeGamePB = new QPushButton("Resume Game");
    saveGamePB = new QPushButton("Save Game");
    mainMenuPB = new QPushButton ("Quit Game");
    exitPB = new QPushButton("Exit");

    resumeGamePB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    saveGamePB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    mainMenuPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    exitPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");

    warningCircle = new QGraphicsPixmapItem;
    warningCircle->setPixmap(QPixmap(string("photos/circle").c_str()).scaled(230,230));
    warningCircle->hide();

    pauseMenuLayout = new QVBoxLayout;
    pauseMenuLayout->addWidget(resumeGamePB);
    pauseMenuLayout->addWidget(saveGamePB);
    pauseMenuLayout->addWidget(mainMenuPB);
    pauseMenuLayout->addWidget(exitPB);

    pauseMenu->setLayout(pauseMenuLayout);
    pauseMenu->setGeometry(topRight+200,topLeft+220,300,300);
    pauseMenu->hide();

    gameOverMenu=new QWidget;
    gameOverMenu->setGeometry(topRight+200,topLeft+220,300,300);

    username=user;
    highscore=high;
    playingMode=mode;

    QString str = playingMode?"Police":"Robber";
    scoreLabel = new QLabel(str+"'s score = 0");
    policeNearbyLabel = new QLabel("Police Nearby!");
    policeNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
    policeNearbyLabel->hide();
    robberNearbyLabel = new QLabel("Robber Nearby!");
    robberNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
    robberNearbyLabel->hide();
    scoreLabel->setGeometry(topRight+800,topLeft+70,180,20);

    map = Map::constructMap(mapType);
    robber = new Robber(true,scene,playingMode);
    policeDep = new PoliceDep();

    if (playingMode==0)
        score=0;
    else {
        score=(map->sumAll());
    }
    robberScore=0;
    movedCount=30;
    moneyStolen=0;

    if(playingMode)
    {
        for(unsigned int i=0; i<policeDep->getSize(); i++)
            policeDep->getPolice(i)->setFlag(QGraphicsItem::ItemIsFocusable, true);
        robber->setFlag(QGraphicsItem::ItemIsFocusable, false);
    }
    else
    {
        robber->setFlag(QGraphicsItem::ItemIsFocusable, true);
        for(unsigned int i=0; i<policeDep->getSize(); i++)
            policeDep->getPolice(i)->setFlag(QGraphicsItem::ItemIsFocusable, false);
    }


    for(unsigned int i=0; i<map->mapRoads.size(); i++)
        scene->addItem(map->mapRoads[i]);
    for(unsigned int i=0; i<map->mapBuildings.size(); i++)
    {
        scene->addItem(map->mapBuildings[i]);
        scene->addWidget(map->mapBuildings[i]->getBuildingInfo());
    }
    for(unsigned int i=0; i<map->mapGates.size(); i++)
        scene->addItem(map->mapGates[i]);

    Police* p1 = new Police();
    Police* p2 = new Police();
    Police* p3 = new Police();
    Police* p4 = new Police();
    p1->setPos(300,500);
    p2->setPos(300,100);
    p3->setPos(100,300);
    selectedPolice=0;
    if (mapType==2)
    {
        p1->setPos(605,805);
        p2->setPos(55,405);
        p3->setPos(995,755);
        p4->setPos(405,995);
        policeDep->addPolice(p4);
    }
    policeDep->addPolice(p1);
    policeDep->addPolice(p2);
    policeDep->addPolice(p3);

    am = new AbstractMap(map);

    if (mapType!=2)
        robber->setPos(QPointF(300,300));
    else robber->setPos(QPointF(605,405));
    if (playingMode)
        robber->hide();
    scene->addItem(robber);
    scene->addItem(warningCircle);

    scene->addWidget(scoreLabel);
    scene->addWidget(policeNearbyLabel);
    scene->addWidget(robberNearbyLabel);

    if (!playingMode)//robber playing
    {
        for (unsigned int i=0; i<policeDep->getSize(); i++)
            policeDep->getPolice(i)->hide();
    }
    policeDep->addPoliceToScene(scene);

    QString s;
    s.setNum(map->winScore);
    targetScoreLabel=new QLabel;
    targetScoreLabel->setGeometry(topRight+800,topLeft+30,180,20);
    targetScoreLabel->setText("Target Score: "+s);
    if (!playingMode)
        scene->addWidget(targetScoreLabel);

    s.setNum(moneyStolen);
    moneyStolenLabel=new QLabel;
    moneyStolenLabel->setGeometry(topRight+800,topLeft+90,180,20);
    moneyStolenLabel->setText("Money Stolen: " +s);
    scene->addWidget(moneyStolenLabel);

    s.setNum(0);
    timeLabel = new QLabel;
    timeLabel->setGeometry(topRight+800,topLeft+50,180,20);
    timeLabel->setText("Time: "+s);
    scene->addWidget(timeLabel);
    isWeekend=false;

    pauseProxyWidget = scene->addWidget(pauseMenu);
    pauseProxyWidget->setZValue(1000);

    this->setScene(scene);
    this->setBackgroundBrush(QBrush(QColor(100,100,247,200), Qt::SolidPattern));
    this->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    this->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->setDragMode(QGraphicsView::ScrollHandDrag);
    this->showMaximized();

    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
    QObject::connect(&circleTimer,SIGNAL(timeout()),this,SLOT(hideCircle()));
    timer.setInterval(2000/33);
    circleTimer.setInterval(2000);//every 2 sec
    timer.start();
    t0 = time(0);

    connect(resumeGamePB, SIGNAL(clicked()),this,SLOT(resumeGame()));
    connect(saveGamePB, SIGNAL(clicked()),this,SLOT(saveGame()));
    connect(mainMenuPB, SIGNAL(clicked()),this,SLOT(mainMenu()));
    connect(exitPB,SIGNAL(clicked()),this, SLOT(exit()));
}

bool PlayWindow::getPlayingMode()
{
    return playingMode;
}

QString PlayWindow::getUsername()
{
    return username;
}

time_t PlayWindow::getTime0()
{
    return t0;
}

Robber* PlayWindow::getRobber()
{
    return robber;
}

PoliceDep* PlayWindow::getPoliceDep()
{
    return policeDep;
}

int PlayWindow::getScore()
{
    return score;
}

int PlayWindow::getMoneyStolen()
{
    return moneyStolen;
}

void PlayWindow::setMoneyStolen(int val)
{
    QString s;
    moneyStolen=val;
    moneyStolenLabel->setText("Money Stolen: " + s.setNum(val));
}

void PlayWindow::setScore(int score)
{
    this->score = score;
}

void PlayWindow::mousePressEvent(QMouseEvent *event)
{
    if (!gameOver && !pauseMenu->isVisible()){
        QGraphicsView::mousePressEvent(event);
        QPointF newPos = event->posF();
        if (map->getType()!=2)
            newPos = QPointF(newPos.x()-467, newPos.y()-86);//normalization.
        else{
            newPos = QPointF(newPos.x()-170+topRight-250, newPos.y()-36+topLeft);//normalization
        }
        if (!playingMode || event->button()==Qt::NoButton)
            return;
        else if (event->button()==Qt::RightButton) {
            Robber* abstractRobber = new Robber(false);
            abstractRobber->setVisible(false);
            abstractRobber->setPos(newPos);
            bool om = abstractRobber->onMap();
            if (om && !policeDep->tPressed)
                policeDep->setLoc(selectedPolice,newPos);
            else if (om && policeDep->tPressed && policeDep->specialRemaining>0){
                allFollowRobber(newPos);//special move
                policeDep->specialRemaining-=1;
            }
            delete abstractRobber;
        }
    }
}

void PlayWindow::keyPressEvent(QKeyEvent *event)
{
    if (gameOver) return;
    switch(event->key())
    {
    case Qt::Key_Escape:

        if(pauseMenu->isVisible())
        {
            t0 += time(0) - pauseTime;
            pauseMenu->hide();
        }
        else
        {
            pauseTime=time(0);
            pauseMenu->setFocus();
            pauseMenu->show();
        }
        break;
    }

    if (playingMode && map->getType()==2){
        switch(event->key()){
        case Qt::Key_Up:
            topLeft-=10;
            targetScoreLabel->setGeometry(800+topRight,topLeft+30,180,20);
            scoreLabel->setGeometry(topRight+800,topLeft+70,180,20);
            robberNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            policeNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            targetScoreLabel->setGeometry(topRight+800,topLeft+30,180,20);
            moneyStolenLabel->setGeometry(topRight+800,topLeft+90,180,20);
            timeLabel->setGeometry(topRight+800,topLeft+50,180,20);
            pauseMenu->setGeometry(topRight+250,topLeft+220,300,300);
            gameOverMenu->setGeometry(topRight+250,topLeft+220,300,300);
            scene->setSceneRect(topRight,topLeft,700,700);
            break;
        case Qt::Key_Down:
            topLeft+=10;
            targetScoreLabel->setGeometry(800+topRight,topLeft+30,180,20);
            scoreLabel->setGeometry(topRight+800,topLeft+70,180,20);
            robberNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            policeNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            targetScoreLabel->setGeometry(topRight+800,topLeft+30,180,20);
            moneyStolenLabel->setGeometry(topRight+800,topLeft+90,180,20);
            timeLabel->setGeometry(topRight+800,topLeft+50,180,20);
            pauseMenu->setGeometry(topRight+250,topLeft+220,300,300);
            gameOverMenu->setGeometry(topRight+250,topLeft+220,300,300);
            scene->setSceneRect(topRight,topLeft,700,700);
            break;
        case Qt::Key_Left:
            topRight -= 10;
            targetScoreLabel->setGeometry(800+topRight,topLeft+30,180,20);
            scoreLabel->setGeometry(topRight+800,topLeft+70,180,20);
            robberNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            policeNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            targetScoreLabel->setGeometry(topRight+800,topLeft+30,180,20);
            moneyStolenLabel->setGeometry(topRight+800,topLeft+90,180,20);
            timeLabel->setGeometry(topRight+800,topLeft+50,180,20);
            pauseMenu->setGeometry(topRight+250,topLeft+220,300,300);
            gameOverMenu->setGeometry(topRight+250,topLeft+220,300,300);
            scene->setSceneRect(topRight,topLeft,700,700);
            break;
        case Qt::Key_Right:
            topRight+=10;
            targetScoreLabel->setGeometry(800+topRight,topLeft+30,180,20);
            scoreLabel->setGeometry(topRight+800,topLeft+70,180,20);
            policeNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            robberNearbyLabel->setGeometry(topRight+800,topLeft+110,180,20);
            targetScoreLabel->setGeometry(topRight+800,topLeft+30,180,20);
            moneyStolenLabel->setGeometry(topRight+800,topLeft+90,180,20);
            timeLabel->setGeometry(topRight+800,topLeft+50,180,20);
            pauseMenu->setGeometry(topRight+250,topLeft+220,300,300);
            gameOverMenu->setGeometry(topRight+250,topLeft+220,300,300);
            scene->setSceneRect(topRight,topLeft,700,700);
            break;
        }
    }
    if(playingMode) //select police.
    {
        int oldVal = selectedPolice;
        switch(event->key()) {
        case Qt::Key_1:
            selectedPolice=0;
            break;
        case Qt::Key_2:
            selectedPolice=1;
            break;
        case Qt::Key_3:
            selectedPolice=2;
            break;
        case Qt::Key_4:
            selectedPolice=3;
            break;
        case Qt::Key_5:
            selectedPolice=4;
            break;
        case Qt::Key_6:
            selectedPolice=5;
            break;
        case Qt::Key_7:
            selectedPolice=6;
            break;
        case Qt::Key_8:
            selectedPolice=7;
            break;
        case Qt::Key_9:
            selectedPolice=8;
            break;
        case Qt::Key_0:
            selectedPolice=9;
            break;
        case Qt::Key_T:
            policeDep->tPressed=true;
            break;
        case Qt::Key_F:
            qreal x = policeDep->getPolice(selectedPolice)->pos().x();
            qreal y = policeDep->getPolice(selectedPolice)->pos().y();
            for (unsigned int i=0; i<map->mapFlags.size(); i++) {
                if (Functions::calculateDistance(map->mapFlags[i]->pos(),QPointF(x,y)) < 10) {
                    scene->removeItem(map->mapFlags[i]);
                    map->mapFlags.erase(map->mapFlags.begin()+i);
                    return;
                }
            }
            PlayWindow::map->setFlag(x,y);
            break;
        }
        selectedPolice=(selectedPolice>=policeDep->getSize())?oldVal:selectedPolice;
    }
    else
    {
        if(event->key()==Qt::Key_Right)
            robber->setMovingRight(true);

        if(event->key()==Qt::Key_Left)
            robber->setMovingLeft(true);

        if(event->key()==Qt::Key_Up)
            robber->setMovingUp(true);

        if(event->key()==Qt::Key_Down)
            robber->setMovingDown(true);

        if (event->key()==Qt::Key_Space)    //trying to steal
        {
            if(robber->getAbleToSteal())
            {
                pair<int,int> add = map->tryToSteal(robber->pos());
                if(add.first>0)
                {
                    moneyStolen += add.first;
                    incrementScore(add.first);
                    QString s;
                    s.setNum(score);
                    s="Robber's score = "+s;
                    scoreLabel->setText(s);
                    s.setNum(moneyStolen);
                    s="Money Stolen: " +s;
                    moneyStolenLabel->setText(s);

                }
                if (add.second==1 || add.second==4)//people in house or guard in neighborhood
                {
                    pair<double,int> nearestPolice = findNearestPolice(robber->pos());
                    policeDep->setLoc(nearestPolice.second,robber->pos());
                }
                else if (add.second==2) {
                    showGameOverScreen(playingMode);
                }
            }
        }
        if(event->key()==Qt::Key_F){     //inserting
            qreal x = robber->pos().x();
            qreal y = robber->pos().y();
            for (unsigned int i=0; i<map->mapFlags.size(); i++) {
                if (Functions::calculateDistance(map->mapFlags[i]->pos(),QPointF(x,y)) < 10) {
                    scene->removeItem(map->mapFlags[i]);
                    map->mapFlags.erase(map->mapFlags.begin()+i);
                    return;
                }
            }
            map->setFlag(x,y);
        }

        //check if robber is near a gate and has enough money
        bool didWin = map->tryToWin(robber->pos(),score);
        if (didWin) {
            showGameOverScreen(!playingMode);
        }
    }
}

void PlayWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (gameOver) return;
    if (playingMode){
        if (event->key()==Qt::Key_T)
            policeDep->tPressed=false;
    }
    else
    {
        if(event->key()==Qt::Key_Right)
            robber->setMovingRight(false);

        if(event->key()==Qt::Key_Left)
            robber->setMovingLeft(false);

        if(event->key()==Qt::Key_Up)
            robber->setMovingUp(false);

        if(event->key()==Qt::Key_Down)
            robber->setMovingDown(false);
    }
}

void PlayWindow::decrementTime0(int amount)
{
    t0 = t0 - amount;
}

void PlayWindow::displayNearbyBuildings()
{
    if(playingMode==0)
    {
        for(unsigned int i=0; i<map->mapBuildings.size(); i++)
        {
            if(Functions::calculateDistance(robber->pos(),map->mapBuildings[i]->pos()) < 100 && !map->mapBuildings[i]->getBuildingInfo()->isVisible())
            {
                map->mapBuildings[i]->getBuildingInfo()->show();
            }
            else if(Functions::calculateDistance(robber->pos(),map->mapBuildings[i]->pos()) > 100 && map->mapBuildings[i]->getBuildingInfo()->isVisible())
                map->mapBuildings[i]->getBuildingInfo()->hide();
        }
    }
    else
    {
        for(unsigned int i=0; i<map->mapBuildings.size(); i++)
        {
            if(!map->mapBuildings[i]->getBuildingInfo()->isVisible())
            {
                map->mapBuildings[i]->getBuildingInfo()->show();
            }
        }
    }
}

void PlayWindow::incrementScore(int val)
{
    score+=val;
    if (val==-5)
        robberScore+=val;
    else robberScore-=val;
    if (score<0)
        score=0;
    if (robberScore<0)
        robberScore=0;
    QString s;
    s.setNum(score);
    QString str;
    str=playingMode?"Police":"Robber";
    s=str+"'s score = "+s;
    scoreLabel->setText(s);
}

pair<double,unsigned int> PlayWindow::findNearestPolice(QPointF loc)
{
    double dist;
    double minDist = 99999999;
    unsigned int nearestPolice=0;

    for(unsigned int i=0; i < policeDep->getSize(); i++)
    {
        dist = Functions::calculateDistance(loc,policeDep->getPolice(i)->pos());
        if(dist < minDist)
        {
            minDist = dist;
            nearestPolice = i;
        }
    }

    return make_pair(minDist,nearestPolice);
}

void PlayWindow::moveOpponent()
{
    pair<double,unsigned int> p = findNearestPolice(robber->pos());
    double dist = p.first;

    if (playingMode)
        moveRobber();
    //120 is equivalent to 60 meters
    if(playingMode && dist<=120)
    {
        robber->show();
        if(dist<80 && !robberNearbyLabel->isVisible())
            robberNearbyLabel->show();
        else if(dist>80 && robberNearbyLabel->isVisible())
            robberNearbyLabel->hide();
    }
    else if (playingMode && dist>120)
        robber->hide();
    else if (!playingMode && dist<120) //robber playing
    {
        allFollowRobber(robber->pos());
        if(dist<80 && !policeNearbyLabel->isVisible())
            policeNearbyLabel->show();
        else if(dist>80 && policeNearbyLabel->isVisible())
            policeNearbyLabel->hide();
    }
}

void PlayWindow::movePoliceDep() //move all police members
{
    for (unsigned int i=0; i<policeDep->getSize(); i++) {
        QPointF policePos = policeDep->getPolice(i)->pos();
        QPointF wantedPos = policeDep->getLoc(i);//place police wants to go to
        QPointF robberPos = robber->pos();
        if (!playingMode && Functions::calculateDistance(robberPos,policePos)>120)
            policeDep->getPolice(i)->hide();
        if (Functions::calculateDistance(policePos,wantedPos)>10) {
            QPointF newPos = am->BFS(policePos,wantedPos);
            if (newPos.x()==-50 && newPos.y()==-50) continue;
            policeDep->getPolice(i)->setPos(newPos);
            policePos = policeDep->getPolice(i)->pos();
        }
        else {
            if (playingMode)
                policeDep->setLoc(i,policePos); //stop him moving until next click
            else {//if playing as robber, choose random building for the policeman to go check
                int buildingIndex = rand()%(map->mapBuildings.size());
                policeDep->setLoc(i,map->buildingEntrances[buildingIndex]);
            }
        }
        //check for game over
        if (Functions::calculateDistance(robber->pos(),policePos) < 27)
        {
            showGameOverScreen(playingMode); //robber lost
        }
    }
}

void PlayWindow::update()
{
    if(!pauseMenu->isVisible() && !gameOver)
    {
        robber->update();
        moveOpponent();
        movePoliceDep();
        displayNearbyBuildings();
        addNewMapFlags();
        updateTime();
    }
}

void PlayWindow::allFollowRobber(QPointF loc)
{
    for(unsigned int i=0; i<policeDep->getSize(); i++)
    {
        if (!playingMode){
            if (Functions::calculateDistance(policeDep->getPolice(i)->pos(),robber->pos())<120)
                policeDep->getPolice(i)->show();
        }
        policeDep->setLoc(i,loc);
    }
}

void PlayWindow::moveRobber()
{
    QPointF robberPos = robber->pos();
    if (movedCount==30){
        am->fillRobberGrid(policeDep);
        movedCount=0;
    }
    movedCount++;
    //Dijkstra chooses the best building/gate to go to while avoiding visible police
    QPointF newPos= am->dijkstra(robberPos);
    if (newPos==robberPos){
        pair<int,int> money = map->tryToSteal(robberPos);
        bool lost = map->tryToWin(robberPos,robberScore);//robber tries to win
        if (lost){
            showGameOverScreen(false);
        }
        if (money.first>0){
            incrementScore(-money.first);
            moneyStolen+=money.first;
            QString s;
            s.setNum(moneyStolen);
            moneyStolenLabel->setText("Money Stolen: "+s);
            if (money.second==1 || money.second==4) //in-home or guard nearby
            {
                circleTimer.stop();
                circleTimer.start();
                int randX = rand()%75;//area around it
                randX = (rand()%2)==0?randX:-randX;
                int randY = rand()%75;
                randY = (rand()%2)==0?randY:-randY;
                int x = robberPos.x()+randX;
                int y = robberPos.y()+randY;
                QRectF r = warningCircle->boundingRect();
                warningCircle->setPos(x-r.width()/2,y-r.height()/2);
                warningCircle->show();
            }
        }
    }
    else robber->setPos(newPos);
}

void PlayWindow::addNewMapFlags()
{
    for (unsigned int i=0; i<map->mapFlags.size(); i++)
    {
        if(map->mapFlags[i]->scene()==0)
            scene->addItem(map->mapFlags[i]);
    }
}


void PlayWindow::updateTime()
{
    QString s;
    int currTime = time(0)-t0;

    s.setNum(currTime);
    if (timeLabel->text()!="Time: "+s+ "\t"+day[(currTime%70)/10])
        incrementScore(-5);
    timeLabel->setText("Time: " + s + "\t" + day[(currTime%70)/10]);
    if ((isWeekend && (currTime)%70==0) || (!isWeekend && (currTime+20)%70==0)) {
        map->flipGuards();
        isWeekend=!isWeekend;
    }
}

void PlayWindow::showGameOverScreen(bool _won)
{
    won=_won;
    gameOver = true;
    if (score>highscore)
    {
        DbHandler::updateHighScore(username,score);
        highscore=score;
    }

    gameOverLayout = new QVBoxLayout;

    QString label = won?"You Win!":"Game Over";
    QString col = won?"green":"red";
    gameOverPB=new QPushButton(label);
    gameOverPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px; color: "+col+";");

    label = (won && map->getType()!=2)?"Next Level":"Retry";
    retryPB = new QPushButton(label);
    retryPB->setStyleSheet("background-color: transparent; font-weight: bold; font-size: 25px;");
    QObject::connect(retryPB,SIGNAL(clicked()),this,SLOT(retry()));

    gameOverLayout->addWidget(gameOverPB);
    gameOverLayout->addWidget(retryPB);
    gameOverLayout->addWidget(mainMenuPB);
    gameOverLayout->addWidget(exitPB);

    gameOverMenu->setLayout(gameOverLayout);
    scene->addWidget(gameOverMenu);
    gameOverMenu->raise();
    gameOverMenu->show();
}

void PlayWindow::retry()
{
    int next;
    if (won)
        next = min(map->getType()+1,2);
    else next = map->getType();
    new PlayWindow(username,highscore,next,playingMode);
    this->close();
}

void PlayWindow::hideCircle()
{
    warningCircle->hide();
}

void PlayWindow::resumeGame()
{
    t0 += time(0) - pauseTime;
    pauseMenu->hide();
}

void PlayWindow::saveGame()
{
    DbHandler::save(this,username);
}

void PlayWindow::mainMenu()
{
    this->close();
    DbHandler::close();
    new MainMenu();
}

void PlayWindow::exit()
{
    //delete pauseMenu;
    DbHandler::close();
    this->close();
}
