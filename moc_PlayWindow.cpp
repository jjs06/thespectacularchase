/****************************************************************************
** Meta object code from reading C++ file 'PlayWindow.h'
**
** Created: Tue May 6 03:32:15 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "PlayWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PlayWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PlayWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      21,   11,   11,   11, 0x0a,
      29,   11,   11,   11, 0x0a,
      42,   11,   11,   11, 0x0a,
      55,   11,   11,   11, 0x0a,
      66,   11,   11,   11, 0x0a,
      77,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PlayWindow[] = {
    "PlayWindow\0\0update()\0retry()\0hideCircle()\0"
    "resumeGame()\0saveGame()\0mainMenu()\0"
    "exit()\0"
};

void PlayWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PlayWindow *_t = static_cast<PlayWindow *>(_o);
        switch (_id) {
        case 0: _t->update(); break;
        case 1: _t->retry(); break;
        case 2: _t->hideCircle(); break;
        case 3: _t->resumeGame(); break;
        case 4: _t->saveGame(); break;
        case 5: _t->mainMenu(); break;
        case 6: _t->exit(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData PlayWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PlayWindow::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_PlayWindow,
      qt_meta_data_PlayWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PlayWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PlayWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PlayWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PlayWindow))
        return static_cast<void*>(const_cast< PlayWindow*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int PlayWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
