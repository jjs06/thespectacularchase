/**
\file MainMenu.h
\brief A class that constructs a GUI MainMenu for initial user interaction with the game.

Allows the user to start a new game, resume an old one, choose the map, and so on.
*/

#ifndef MAINMENU_H
#define MAINMENU_H

#include <QtGui>
#include <iostream>
using namespace std;

#include "DbHandler.h"
#include "PlayWindow.h"

class MainMenu: public QObject
{
    Q_OBJECT

private:
    QGraphicsView* view;
    QGraphicsScene* scene;
    QPushButton *newGamePB;
    QPushButton *loadGamePB;
    QPushButton *instPB;
    QPushButton *cityPB;
    QPushButton *acctPB;
    QPushButton *exitPB;
    QRadioButton *robberRB;
    QRadioButton *policeRB;
    QHBoxLayout *playingModeBL;
    QLineEdit *uLine;
    QWidget *playingModeW;
    QString username;
    QString loadgame;
    int mapType;
    QComboBox *acctCBPointer;
public:
    /**
      Constructs a Main Menu for the game.
      */
    MainMenu();
    /**
      Retrive the playing mode
    */
    bool getPM();
public slots:
    /**
      Instantiate a New Game according to the selected playing mode and city
    */
    void newGame();
    /**
      Access the Saved Games scene to select a Game to Load
    */
    void loadGame();
    /**
      Access the Instructions scene to view the Instructions on how to play the game
    */
    void inst();
    /**
      Access the Select City scene to select a city from a drop down menu
    */
    void city();
    /**
      Exit the game
    */
    void exit();
    /**
      Go back to the Main Menu
    */
    void back();
    /**
      Allow the player to choose his user account
    */
    void acct();
    /**
      Load the selected game
    */
    void loadSelectedGame();

    /**
      Updates the mapType member when the user changes choice in the city combo box.
      */
    void updateMapType(int);

    /**
      Updates the username member to the given username string.
      */
    void updateUsername(QString);

    /**
      Updates the loadgame member to the given loadgame string.
      */
    void updateLoadGameString(QString);

    /**
      Adds a user to the combo box and the database when the user creates a new username.
      If the username is already present, the database and combo box are not updated.
      */
    void addUser();
};

#endif // MAINMENU_H
